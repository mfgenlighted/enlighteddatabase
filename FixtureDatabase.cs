﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

using MySql.Data.MySqlClient;

//---- revisions --------------
// 3/23/2018
//  made a const connectionTimeout and set it to 60 sec.
//  better exception messages
//  added the fields error_stepnumber, workorder, and hla_productcode in dut_results
// 3/30/2018
//  added error step number and error step name to the close database function.

namespace FixtureDatabase
{

//=========================================================
//  Properties
//
//  Has the data for the database. Has the following properties
//      DBInfoUser
//      DBInfoPassword
//      DBInfoServer - the IP or server name
//      DBInfoDatabase - Database name to connect to
//      DBInfoRecordOption -  (0)OnTheFly -> records the data into the database as it is created.
//                            (1)NONE -> no data will be recorded in the database.
//                            (2)END -> data will be recorded when the Finish routine is called.
//  Has the info for the text report file
//      TextFileDirectory - directory to save the file in. Set to "" to not write out a file.
//  Has station info
//      TestOperator - name of the test operator. Gets stored in results database
//      StationName - name of the test station. Gets stored in the results database
//
//  Methods
//      ResultData() - top class
//      Dispose()
//      void CreateTestRecord(string serialNumber, string testStation, string limitFileVersion, string limitFileName, string programVersion)
//      void CloseTestRecord(int lastErrorCode, string lastErrorMsg)
//      void CreateStepRecord(string stepName, int testNumber)
//      void AddStepResult(string propName, string result, string compOperator,
//                  double lowLimit, double highLimit, double value, string units, string format, string reportText, int errorCode)
//      void AddStepResult(string propName, string result, string compOperator, double limit, double value,
//                  string units, string format, string reportText, int errorCode)
//      void AddStepResult(string propName, string result, string compOperator, string compString, string value,
//                  string reportText, int errorCode)
//      void AddStepResult(string propName, string result, string reportText, int errorCode)
//      void AddStepResult(string result, string reportText, int errorCode)
//      void AddStepRef(string propName, string refValue, string units, string refFormat, string reportText, int errorCode)
//      

    public class ResultData : IDisposable
    {
        public enum type { type_ref, type_pass_fail, type_numeric, type_string };
        public enum PropType { type_ref, type_numeric, type_numericRange, type_string };
        public enum DBRecordType { OnTheFly, NONE, END };
        public const string TEST_PASSED = "PASS";
        public const string TEST_FAILED = "FAIL";

        //--------------------------------------
        //  properties
        //--------------------------------------
        public string DBInfoUser {get; set;}
        public string DBInfoPassword {get; set;}
        public string DBInfoServer { get; set;}
        public string DBInfoDatabase { get; set;}
        public DBRecordType DBInfoRecordOption { get; set;}
        public string TextFileInfoDirectory {get; set;}
        public bool RecordTextFile { get; set; }
        public string TestOperator {get; set;}
        public string StationName { get; set; }
        public string TestOperation { get; set; }
        public string WorkOder { get; set; }
        public string HLAProductCode { get; set; }

        //------------------------------------------
        //  storage for test data
        //----------------------------------------
        private dutResultRecord ResultDatabaseData;          // memory version of the data
        private bool dataHasBeenRecorded = true;            // set to true once CloseTestRecord has been called
        private class dutResultRecord
        {
            public Guid id { get; set; }
            public string serialNumber{get; set;}
            public DateTime testStartTime{get; set;}
            public int executionTime{get; set;}
            public string testOperator{get; set;}
            public string stationName{get; set;}
            public string testOperation { get; set; }
            public string result{get; set;}
            public string limitFileVersion{get; set;}
            public string limitFileName{get; set;}
            public string programVersion{get; set;}
            public int errorCode{get; set;}
            public string errorMessage{get; set;}
            public int error_stepnumber { get; set; }
            public string error_stepname { get; set; }
            public int error_testnumber { get; set; }
            public string workorder { get; set; }
            public string hla_productcode { get; set; }
            public List<stepdResultRecord> StepResult{get; set;}
            public dutResultRecord()
            {
                StepResult = new List<stepdResultRecord>();
            }
            
        }

        private class stepdResultRecord
        {
            public Guid id { get; set; }
            public Guid dutResultId{get; set;}
            public string stepName{get; set;}                // step name
            public int testNumber{get; set;}
            public int stepType{get; set;}                // undefined right now
            public int stepIndex { get; set; }                 // step number
            public string stepResult{get; set;}              // filled in by adding results
            public DateTime stepStarttime{get; set;}         // time that the stucture was created
            public string reportText{get; set;}
            public int errorCode{get; set;}
            public string errorMsg{get; set;}
            public List<propResultRecord> PropResult{get; set;}
            public stepdResultRecord()
            {
                PropResult = new List<propResultRecord>();
            }
        }

        private class propResultRecord
        {
            public Guid id{get; set;}
            public Guid stepResultId{get; set;}
            public uint propIndex { get; set; }
            public string name{get; set;}
            public int type{get; set;}
            public string data{get; set;}
            public string displayFormat{get; set;}
            public string units{get; set;}
            public string result{get; set;}
            public stringPropRecord StringProp{get; set;}
            public numlimitPropRecord NumlimitProp{get; set;}
        }

        private class stringPropRecord
        {
            public Guid id { get; set; }
            public Guid propResultId { get; set; }
            public string compOperator{get; set;}
            public string compString{get; set;}
            public string status{get; set;}
        }

        private class numlimitPropRecord
        {
            public Guid id { get; set; }
            public Guid propResultId { get; set; }
            public string compOperator{get; set;}
            public double highLimit{get; set;}
            public double lowLimit{get; set;}
            public string units{get; set;}
            public string status{get; set;}
        }



        private int currentStepResultIndex = 0;
        private int currentPropResultIndex = 0;
        private string lastFailMsg = string.Empty;

        private  MySqlConnection connection = new MySqlConnection();        // used if saving database on the fly

        private bool StepFailed = true;
        private bool disposed = false;


        //---------- constants
        const int connectionTimeout = 60;               // database connection timeout in secs


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (connection.State == ConnectionState.Open)     // if the database is still open
                    {
                        MessageBox.Show("Result database is still open. CloseTestRecord has not been called");
                        throw new Exception("CloseTestRecord has not been called");
                    }
                    if (dataHasBeenRecorded == false)     // if the database is still open
                    {
                        MessageBox.Show("Result has not been wrote out. CloseTestRecord has not been called");
                        throw new Exception("CloseTestRecord has not been called");
                    }
                    disposed = true;
                }
            }
        }

        //==========================================================================================
        // Create and Finish routines
        //
        //  The Create is the first thing that is called to start
        //  recording the data. It will create the Test Result head record.
        //===========================================================================================

        //-----------------------------------------------------------
        // CreateTestRecord
        //  Head of the test record
        public void CreateTestRecord(string serialNumber, string testStation, string limitFileVersion, string limitFileName, string programVersion,
                                        string workorder, string hla_productcode)
        {
            if (DBInfoRecordOption != DBRecordType.NONE)       // if the data is to be recorded in a database
            {
                if ((DBInfoDatabase == "") | (DBInfoServer == "") | (DBInfoUser == ""))
                    throw new Exception("Can not open the database. One or more database properties are not set");

                // make sure the table can be opened
                connection.ConnectionString = "Server=" + DBInfoServer + ";Database=" + DBInfoDatabase + ";uid=" + DBInfoUser + ";password=" +
                                                    DBInfoPassword + ";" + "Connect Timeout=" + connectionTimeout + ";";
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error opeing database. \n Server= " + DBInfoServer + " Database= " + DBInfoDatabase + " User= " + DBInfoUser + "\n" + ex.Message);
                }
                connection.Close();
            }

            StepFailed = false;
            dataHasBeenRecorded = false; 

            ResultDatabaseData = new dutResultRecord();                     // create the stucture for this run

            ResultDatabaseData.id = Guid.NewGuid();
            ResultDatabaseData.testOperator = TestOperator;
            ResultDatabaseData.serialNumber = serialNumber;
            ResultDatabaseData.testStartTime = DateTime.UtcNow;
            ResultDatabaseData.stationName = StationName;
            ResultDatabaseData.testOperation = TestOperation;
            ResultDatabaseData.result = "Testing";                          // set to 'Testing'. Will be able to tell if the record was closed correctly
            ResultDatabaseData.limitFileVersion = limitFileVersion;
            ResultDatabaseData.limitFileName = limitFileName;
            ResultDatabaseData.programVersion = programVersion;
            ResultDatabaseData.workorder = workorder;
            ResultDatabaseData.hla_productcode = hla_productcode;
            

            currentStepResultIndex = 0;


            if (DBInfoRecordOption == DBRecordType.OnTheFly)                // if recording data to database on the fly
            {
                connection.ConnectionString = "Server=" + DBInfoServer + ";Database=" + DBInfoDatabase + ";uid=" + DBInfoUser + ";password=" +
                                                    DBInfoPassword + ";" + "Connect Timeout=" + connectionTimeout + ";";
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error opeing database. \n Server= " + DBInfoServer + " Database= " + DBInfoDatabase + " User= " + DBInfoUser + "\n" + ex.Message);
                }

                MySqlCommand cmd;
                try
                {

                    cmd = connection.CreateCommand();
                    cmd.CommandText = "INSERT INTO dut_result(id, serial_number, start_test_date, execution_time, operator, station, result, limit_file_version, " +
                                        "limit_file_name, program_version, test_operation, workorder, hla_productcode)" +
                                        "VALUES(@ID, @SN, @SD, @ExTime, @Operator, @Station, @Result, @LimitFileVersion, " +
                                        "@LimitFileName, @ProgramVersion, @TestOperation, " +
                                        "@Workorder, @HLA_productcode)";
                    cmd.Parameters.AddWithValue("@ID", ResultDatabaseData.id.ToString());
                    cmd.Parameters.AddWithValue("@SN", ResultDatabaseData.serialNumber);
                    cmd.Parameters.AddWithValue("@SD", ResultDatabaseData.testStartTime);
                    cmd.Parameters.AddWithValue("@ExTime", (DateTime.UtcNow - ResultDatabaseData.testStartTime).TotalSeconds);
                    cmd.Parameters.AddWithValue("@Operator", ResultDatabaseData.testOperator);
                    cmd.Parameters.AddWithValue("@Station", ResultDatabaseData.stationName);
                    cmd.Parameters.AddWithValue("@Result", ResultDatabaseData.result);
                    cmd.Parameters.AddWithValue("@LimitFileVersion", ResultDatabaseData.limitFileVersion);
                    cmd.Parameters.AddWithValue("@LimitFileName", ResultDatabaseData.limitFileName);
                    cmd.Parameters.AddWithValue("@ProgramVersion", ResultDatabaseData.programVersion);
                    cmd.Parameters.AddWithValue("@TestOperation", ResultDatabaseData.testOperation);
                    cmd.Parameters.AddWithValue("@Workorder", ResultDatabaseData.workorder);
                    cmd.Parameters.AddWithValue("@HLA_productcode", ResultDatabaseData.hla_productcode);


                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error inserting new data to database. " + ex.Message);
                }
            }
        }


        //-----------------------------------------------------------
        // CloseTestRecord
        //  Update the Test Record with pass/fail and execution time.
        //
        public void CloseTestRecord(int lastErrorCode, string lastErrorMsg, int lastErrorStep, string lastErrorStepName, int lastErrorTestNumber)
        {
            MySqlCommand cmd;
            double testtime;

            if (ResultDatabaseData == null)     // if nothing saved, do nothing
            {
                dataHasBeenRecorded = true;
                return;
            }

            testtime = (DateTime.UtcNow - ResultDatabaseData.testStartTime).TotalSeconds;

            ResultDatabaseData.executionTime = (int)testtime;
            ResultDatabaseData.result = StepFailed ? "FAIL" : "PASS";
            ResultDatabaseData.errorCode = (int)lastErrorCode;
            ResultDatabaseData.errorMessage = lastErrorMsg;
            ResultDatabaseData.error_stepname = lastErrorStepName;
            ResultDatabaseData.error_stepnumber = lastErrorStep;
            ResultDatabaseData.error_testnumber = lastErrorTestNumber;

            if (RecordTextFile)      // if set to record the data in a text file
            {
                // create a file name
                string filename = TextFileInfoDirectory + @"\" + ResultDatabaseData.serialNumber + "_" + DateTime.UtcNow.ToString("o").Replace(':', '_') + ".txt";
                WriteResultDataToTextFile(filename);
            }


            if (DBInfoRecordOption == DBRecordType.OnTheFly)        // if recording database on the fly
            {
                if (connection.State == ConnectionState.Open)
                {
                    cmd = connection.CreateCommand();
                    // need to update the dut_result record
                    //cmd.CommandText = "UPDATE dut_result SET dut_result.result = " + (StepFailed ? "'FAIL'" : "'PASS'") + " WHERE id = '" + currentdut_resultID.ToString() + "'";
                    cmd.CommandText = "UPDATE dut_result SET dut_result.result = @STEPFAILED WHERE id = @ID";
                    cmd.Parameters.AddWithValue("@STEPFAILED", StepFailed ? "FAIL" : "PASS");
                    cmd.Parameters.AddWithValue("@ID", ResultDatabaseData.id.ToString());
                    cmd.ExecuteNonQuery();
                    //cmd.CommandText = "UPDATE dut_result SET dut_result.execution_time = " + testtime.ToString() + " WHERE id = '" + currentdut_resultID.ToString() + "'";
                    cmd.CommandText = "UPDATE dut_result SET dut_result.execution_time = @TESTTIME WHERE id = @ID";
                    cmd.Parameters.AddWithValue("@TESTTIME", testtime);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE dut_result SET dut_result.error_code = '" + lastErrorCode.ToString() + "' WHERE id = '" + ResultDatabaseData.id.ToString() + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE dut_result SET dut_result.error_message = '" + lastErrorMsg + "' WHERE id = '" + ResultDatabaseData.id.ToString() + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE dut_result SET dut_result.error_stepnumber = '" + lastErrorStep.ToString() + "' WHERE id = '" + ResultDatabaseData.id.ToString() + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE dut_result SET dut_result.error_stepname = '" + lastErrorStepName + "' WHERE id = '" + ResultDatabaseData.id.ToString() + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE dut_result SET dut_result.error_testnumber = '" + lastErrorTestNumber.ToString() + "' WHERE id = '" + ResultDatabaseData.id.ToString() + "'";
                    cmd.ExecuteNonQuery();

                    connection.Close();         // all done with the database so close it
                    connection.Dispose();
                }
            }
            if (DBInfoRecordOption == DBRecordType.END)
            {
                WriteResultDataToDB();
            }

            ResultDatabaseData = null;
            dataHasBeenRecorded = true;
        }


        //======================================================
        //      helpers - saves data in database and List(memory) records
        //
        //  

        /// <summary>
        /// Will store the step result data in the ResultDatabaseData.StepResult list.
        /// </summary>
        /// <param name="stepResultID">current step result record id</param>
        /// <param name="dutResultID">the result id that this record is linked to</param>
        /// <param name="stepName">name of the test</param>
        /// <param name="testNumber">test number of the test</param>
        /// <param name="stepType">type of step.</param>
        /// <param name="stepResultIndex">index number for this step</param>
        /// <param name="result">result of the test</param>
        /// <param name="reportText">report text from test</param>
        /// <param name="errorCode">error code from test</param>
        private void SaveStepResultListRecord(Guid stepResultID, Guid dutResultID, string stepName, int testNumber,
                                int stepType, int stepResultIndex, string result, string reportText, int errorCode)
        {
            stepdResultRecord sr = new stepdResultRecord();
            sr.id = stepResultID;
            sr.dutResultId = dutResultID;
            sr.stepName = stepName;
            sr.testNumber = testNumber;
            sr.stepType = stepType;
            sr.stepIndex = stepResultIndex;
            sr.stepResult = result;
            sr.reportText = reportText;
            sr.errorCode = (int)errorCode;
            sr.stepStarttime = DateTime.UtcNow;
            ResultDatabaseData.StepResult.Add(sr);
        }

        /// <summary>
        /// Will add the last record of the ResultDatabaseData.StepResult list in the database to step_result.
        /// The top test record result will not be updated with this code. CloseTestRecord needs to be called
        /// to update the dut test result and error codes.
        /// </summary>
        private void SaveStepResultDBRecord(MySqlConnection lclconn)
        {
            MySqlCommand cmd;
            cmd = lclconn.CreateCommand();

            stepdResultRecord sr = ResultDatabaseData.StepResult.Last();

            cmd.CommandText = "INSERT INTO step_result(id, dut_result_id, name, test_number, type, stepindex, result, report_text, error_code) " +
                                "VALUES(@ID, @dut_result, @StepName, @TestNumber, @StepType, @StepIndex, @StepResult, @ReportText, @ErrorCode)";
            cmd.Parameters.AddWithValue("@ID", sr.id);
            cmd.Parameters.AddWithValue("@dut_result", sr.dutResultId);
            cmd.Parameters.AddWithValue("@StepName", sr.stepName);
            cmd.Parameters.AddWithValue("@TestNumber", sr.testNumber);
            cmd.Parameters.AddWithValue("@StepType", sr.stepType);
            cmd.Parameters.AddWithValue("@StepIndex", sr.stepIndex);    // inc the step index
            cmd.Parameters.AddWithValue("@StepResult", sr.stepResult);
            cmd.Parameters.AddWithValue("@ErrorCode", sr.errorCode);
            cmd.Parameters.AddWithValue("@ReportText", sr.reportText);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Will add the passed step record to step_result.
        /// </summary>
        private void SaveStepResultDBRecord(MySqlConnection lclconn, stepdResultRecord sr)
        {
            MySqlCommand cmd = lclconn.CreateCommand();

            cmd.CommandText = "INSERT INTO step_result(id, dut_result_id, name, test_number, type, stepindex, result, report_text, error_code, error_message, test_time) " +
                                "VALUES(@ID, @dut_result, @StepName, @TestNumber, @StepType, @StepIndex, @StepResult, @ReportText, @ErrorCode, @ErrorMessage, @TestTime)";
            cmd.Parameters.AddWithValue("@ID", sr.id);
            cmd.Parameters.AddWithValue("@dut_result", sr.dutResultId);
            cmd.Parameters.AddWithValue("@StepName", sr.stepName);
            cmd.Parameters.AddWithValue("@TestNumber", sr.testNumber);
            cmd.Parameters.AddWithValue("@StepType", sr.stepType);
            cmd.Parameters.AddWithValue("@StepIndex", sr.stepIndex);    // inc the step index
            cmd.Parameters.AddWithValue("@StepResult", sr.stepResult);
            cmd.Parameters.AddWithValue("@ErrorCode", sr.errorCode);
            cmd.Parameters.AddWithValue("@ErrorMessage", sr.errorMsg);
            cmd.Parameters.AddWithValue("@ReportText", sr.reportText);
            cmd.Parameters.AddWithValue("@TestTime", sr.stepStarttime);
            cmd.ExecuteNonQuery();
        }


        /// <summary>
        /// Save the Prop Result Record in the ResultDatabaseData list. If the result is not PASS, the parent record result will be updated.
        /// </summary>
        /// <param name="stepResultID">Step record ID this is linked to</param>
        /// <param name="propResultID">ID for this prop result</param>
        /// <param name="propResultIndex">index of prop result</param>
        /// <param name="propName">name of the prop</param>
        /// <param name="propType">type of prop this is</param>
        /// <param name="result">result of the test that this prop was tested for</param>
        /// <param name="value">value of the prop</param>
        /// <param name="units">units of the prop</param>
        /// <param name="format">format of the prop</param>
        private void SavePropResultTextRecord(Guid stepResultID, Guid propResultID, int propResultIndex, string propName,
                        type propType, string result, double value, string units, string format)
        {
            propResultRecord pr = new propResultRecord();       // make a new record
            pr.stepResultId = stepResultID;
            pr.id = propResultID;
            pr.propIndex = (uint)propResultIndex;
            pr.name = propName;
            pr.type = (int)propType;
            pr.data = value.ToString(format);
            pr.displayFormat = format;
            pr.units = units;
            pr.result = result;
            ResultDatabaseData.StepResult.Last().PropResult.Add(pr);    // add to the list
        }

        /// <summary>
        /// Save the Prop Result Record in the ResultDatabaseData list.
        /// </summary>
        /// <param name="stepResultID">Guid - Step record ID this is linked to</param>
        /// <param name="propResultID">Guid - ID for this prop result</param>
        /// <param name="propResultIndex">int - index of prop result</param>
        /// <param name="propName">string - name of the prop</param>
        /// <param name="propType">type - type of prop this is</param>
        /// <param name="result">string - result of the test that this prop was tested for</param>
        /// <param name="value">string - value of the prop</param>
        /// <param name="units">string - units of the prop</param>
        /// <param name="format">string - format of the prop</param>
        private void SavePropResultTextRecord(Guid stepResultID, Guid propResultID, int propResultIndex, string propName,
                type propType, string result, string value, string units, string format)
        {
            propResultRecord pr = new propResultRecord();
            pr.stepResultId = stepResultID;
            pr.id = propResultID;
            pr.propIndex = (uint)propResultIndex;
            pr.name = propName;
            pr.type = (int)propType;
            pr.data = value;
            pr.displayFormat = format;
            pr.units = units;
            pr.result = result;
            ResultDatabaseData.StepResult.Last().PropResult.Add(pr);

            // if the prop did not PASS, update the parent record
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase) & (DBInfoRecordOption == DBRecordType.OnTheFly)
                                    & !pr.result.Equals("REF", StringComparison.InvariantCultureIgnoreCase))
                ResultDatabaseData.StepResult.Last().stepResult = result;
        }

        /// <summary>
        /// Will save the NumLimitProps in the database. It will get the data from last NumlimitProp of ResultDatabaseData.
        /// </summary>
        private void SaveNumLimitsPropsDBRecord(MySqlConnection lclconn)
        {
            MySqlCommand cmd;
            cmd = lclconn.CreateCommand();

            numlimitPropRecord npr = ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp;

            cmd.CommandText = "INSERT INTO numlimit_prop(id,prop_result_id, comp_operator, high_limit, low_limit, units, status) " +
                                " Values(@ID, @PropResultID, @CompOperator, @HighLimit, @LowLimit, @Units, @Status)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", npr.id);
            cmd.Parameters.AddWithValue("@PropResultID", npr.propResultId);
            cmd.Parameters.AddWithValue("@CompOperator", npr.compOperator);
            cmd.Parameters.AddWithValue("@HighLimit", npr.highLimit);
            cmd.Parameters.AddWithValue("@LowLimit", npr.lowLimit);
            cmd.Parameters.AddWithValue("@Units", npr.units);
            cmd.Parameters.AddWithValue("@Status", npr.status);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Will save the NumLimitProps in the database. It will get the data from last NumlimitProp record passed.
        /// </summary>
        private void SaveNumLimitsPropsDBRecord(MySqlConnection lclconn, numlimitPropRecord npr)
        {
            MySqlCommand cmd = lclconn.CreateCommand();

            cmd.CommandText = "INSERT INTO numlimit_prop(id,prop_result_id, comp_operator, high_limit, low_limit, units, status) " +
                                " Values(@ID, @PropResultID, @CompOperator, @HighLimit, @LowLimit, @Units, @Status)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", npr.id);
            cmd.Parameters.AddWithValue("@PropResultID", npr.propResultId);
            cmd.Parameters.AddWithValue("@CompOperator", npr.compOperator);
            cmd.Parameters.AddWithValue("@HighLimit", npr.highLimit);
            cmd.Parameters.AddWithValue("@LowLimit", npr.lowLimit);
            cmd.Parameters.AddWithValue("@Units", npr.units);
            cmd.Parameters.AddWithValue("@Status", npr.status);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Will save the NumLimitsProp in the ResultDatabaseData list.
        /// </summary>
        /// <param name="id">Guid - id for this record</param>
        /// <param name="propResultID">Guid - id of the Prop record this is linked to</param>
        /// <param name="result">string - results of the test</param>
        /// <param name="compOperator">string - comp operator</param>
        /// <param name="lowLimit">double - low limit</param>
        /// <param name="highLimit">double - high limit</param>
        /// <param name="units">string - units</param>
        private void SaveNumLimitsPropsTextRecord(Guid id, Guid propResultID, string result, string compOperator, double lowLimit, double highLimit, string units)
        {
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp = new numlimitPropRecord();
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.id = id;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.propResultId = propResultID;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.compOperator = compOperator;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.highLimit = highLimit;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.lowLimit = lowLimit;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.units = units;
            ResultDatabaseData.StepResult.Last().PropResult.Last().NumlimitProp.status = result;
       }

        /// <summary>
        /// Save the Prop Result Record in the database. Uses the last Prop Result record of ResultDatabaseData.
        /// If the result is not PASS, the parent record result will be updated.
        /// </summary>
        private void SavePropResultDBRecord(MySqlConnection lclconn)
        {
            MySqlCommand cmd;
            cmd = lclconn.CreateCommand();

            propResultRecord pr = ResultDatabaseData.StepResult.Last().PropResult.Last();

            cmd.CommandText = "INSERT INTO prop_result(id, step_result_id, propindex, name, type, data, display_format, units, result) " +
                                "VALUES(@ID, @StepResultID, @Index, @PropName, @PropType, @PropData, @DisplayFormat, @Units, @Result)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", pr.id.ToString());
            cmd.Parameters.AddWithValue("@StepResultID", pr.stepResultId.ToString());
            cmd.Parameters.AddWithValue("@PropName", pr.name);
            cmd.Parameters.AddWithValue("@PropType", pr.type);
            cmd.Parameters.AddWithValue("@PropData", pr.data);
            cmd.Parameters.AddWithValue("@Units", pr.units);
            cmd.Parameters.AddWithValue("@DisplayFormat", pr.displayFormat);
            cmd.Parameters.AddWithValue("@Index", pr.propIndex);
            cmd.Parameters.AddWithValue("@Result", pr.result);
            cmd.ExecuteNonQuery();

            if (pr.result.Equals("FAIL", StringComparison.OrdinalIgnoreCase) & (DBInfoRecordOption == DBRecordType.OnTheFly)
                                    & !pr.result.Equals("REF", StringComparison.InvariantCultureIgnoreCase))              // if the prop did not PASS
            {
                var currentStepResultID = ResultDatabaseData.StepResult.Last().id;
                cmd.CommandText = "UPDATE step_result SET step_result.result = @Result WHERE step_result.id = @ID";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Result", pr.result);
                cmd.Parameters.AddWithValue("@ID", currentStepResultID);
                cmd.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Save the Prop Result Record in the database. Uses the Prop record passed.
        /// </summary>
        private void SavePropResultDBRecord(MySqlConnection lclconn, propResultRecord pr)
        {
            MySqlCommand cmd = lclconn.CreateCommand();

            cmd.CommandText = "INSERT INTO prop_result(id, step_result_id, propindex, name, type, data, display_format, units, result) " +
                                "VALUES(@ID, @StepResultID, @Index, @PropName, @PropType, @PropData, @DisplayFormat, @Units, @Result)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", pr.id.ToString());
            cmd.Parameters.AddWithValue("@StepResultID", pr.stepResultId.ToString());
            cmd.Parameters.AddWithValue("@PropName", pr.name);
            cmd.Parameters.AddWithValue("@PropType", pr.type);
            cmd.Parameters.AddWithValue("@PropData", pr.data);
            cmd.Parameters.AddWithValue("@Units", pr.units);
            cmd.Parameters.AddWithValue("@DisplayFormat", pr.displayFormat);
            cmd.Parameters.AddWithValue("@Index", pr.propIndex);
            cmd.Parameters.AddWithValue("@Result", pr.result);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Will save the String Prop data in the ResultDatabaseData. Will get its data from the last String prop record.
        /// </summary>
        private void SaveStringPropDBRecord(MySqlConnection lclconn)
        {
            MySqlCommand cmd;
            cmd = lclconn.CreateCommand();

            stringPropRecord sr = ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp;

            cmd.CommandText = "INSERT INTO string_prop(id, prop_result_id, comp_operator, comp_string, status) " +
                                " Values(@ID, @PropResultID, @CompOperator, @CompString, @Status)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", sr.id);
            cmd.Parameters.AddWithValue("@PropResultID", sr.propResultId);
            cmd.Parameters.AddWithValue("@CompOperator", sr.compOperator);
            cmd.Parameters.AddWithValue("@CompString", sr.compString);
            cmd.Parameters.AddWithValue("@Status", sr.status);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Will save the String Prop data in the data passed
        /// </summary>
        private void SaveStringPropDBRecord(MySqlConnection lclconn, stringPropRecord sr)
        {
            MySqlCommand cmd = lclconn.CreateCommand();
            cmd.CommandText = "INSERT INTO string_prop(id, prop_result_id, comp_operator, comp_string, status) " +
                                " Values(@ID, @PropResultID, @CompOperator, @CompString, @Status)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@ID", sr.id);
            cmd.Parameters.AddWithValue("@PropResultID", sr.propResultId);
            cmd.Parameters.AddWithValue("@CompOperator", sr.compOperator);
            cmd.Parameters.AddWithValue("@CompString", sr.compString);
            cmd.Parameters.AddWithValue("@Status", sr.status);
            cmd.ExecuteNonQuery();
        }


        /// <summary>
        /// Will save the String prop data in the ResultDatabaseData list.
        /// </summary>
        /// <param name="id">Guid - id of this record</param>
        /// <param name="propResultID">Guid - id of the parent prop record</param>
        /// <param name="result">string - result of the test</param>
        /// <param name="compOperator">string - compare operator</param>
        /// <param name="compString">string - limit string</param>
        private void SaveStringPropTextRecord(Guid id, Guid propResultID, string result, string compOperator, string compString)
        {
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp = new stringPropRecord();
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp.id = id;
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp.propResultId = propResultID;
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp.compOperator = compOperator;
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp.compString = compString;
            ResultDatabaseData.StepResult.Last().PropResult.Last().StringProp.status = result;
        }


        //------------------------------------------------------
        //  functions that adds the test data
        //
        //  Use CreateStepRecord to setup for results. The Test Result will be set to 'NONE' to
        //      indicate that no results were ever recorded. One of the Add... functions must be 
        //      called to load the actual results of the step.
        //--------------------------------------


        //---------------------------------------------------------
        // SetupStepResult
        //  Sets up the Step Result. AddStepResult function
        //  must be called to write out to the database.
        public void CreateStepRecord(string stepName, int testNumber)
        {
            // make a holder for the test result.
            currentStepResultIndex++;       // starts at 0 when a Test Record is created
            currentPropResultIndex = 0;
            // save data in List
            SaveStepResultListRecord(Guid.NewGuid(), ResultDatabaseData.id, stepName, testNumber, (int)type.type_numeric, currentStepResultIndex, "PASS", "", 0);
            if (DBInfoRecordOption == DBRecordType.OnTheFly)
                SaveStepResultDBRecord(connection);

        }



        //-------------------------------------------------------
        //  AddStepResult overloads
        //  Added result data to the StepResult.
        //
        //  AddStepResult(string propName, string result, string compOperator,
        //                 double lowLimit, double highLimit, double value, string units, string format, string reportText, int errorCode)
        //      ---Numeric high/low test
        //
        //  AddStepResult(string propName, string result, string compOperator,
        //                 double Limit, double value, string units, string format, string reportText, int errorCode)
        //      ---Numeric single value test
        //
        //  AddStepResult(string propName, string result, string compOperator, string compString, string value, string reportText, int errorCode)
        //      ---String compare
        //
        //  AddStepResult(string propName, string result, string reportText, int errorCode)
        //      ---Pass/fail
        //
        //  AddStepResult(string result, string reportText, int errorCode)
        //      ---Pass/Fail with no props. Used for Run type test which have Pass/Fail with not test limits.
        //          This function is called by all the other ones to update the reportText and errorCode
        //          in the step_result record.
        //---------------------------------------------------------------


        //---------------------------------------------------------------
        //  AddStepResult
        /// <summary>
        /// This will add the range numeric Step result.
        /// currentPropResultIndex is set to 0 when the step is created.
        /// </summary>
        /// <param name="propName">Property name</param>
        /// <param name="result">Result of Test</param>
        /// <param name="compOperator">Compare operator</param>
        /// <param name="lowLimit">Low limit value</param>
        /// <param name="highLimit">High limit value</param>
        /// <param name="value">Value that was read</param>
        /// <param name="units">Units of the value</param>
        /// <param name="format">Format of the value</param>
        /// <param name="reportText">Report text from the test</param>
        /// <param name="errorCode">Test error code</param>
        public void AddStepResult(string propName, string result, string compOperator,
            double lowLimit, double highLimit, double value, string units, string format, string reportText, int errorCode)
        {
            Guid propResultID = Guid.NewGuid();
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase))
                StepFailed = true;

            // now create the PropResult record
            try
            {
                currentPropResultIndex++;
                SavePropResultTextRecord(ResultDatabaseData.StepResult.Last().id, propResultID, currentPropResultIndex, propName, type.type_numeric, result, value, units, format);
                // if the prop did not PASS, update the parent record
                if (result.Equals("FAIL", StringComparison.InvariantCultureIgnoreCase))
                {
                    ResultDatabaseData.StepResult.Last().stepResult = result;
                    ResultDatabaseData.StepResult.Last().errorCode = (int)errorCode;
                    ResultDatabaseData.StepResult.Last().errorMsg = reportText;
                }

                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SavePropResultDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating PropResult record. " + ex.Message);
            }

            // now create the NumLimitProps record
            try
            {
                Guid id = Guid.NewGuid();

                SaveNumLimitsPropsTextRecord(id, propResultID, result, compOperator, lowLimit, highLimit, units);
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SaveNumLimitsPropsDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating NumLimitProps record. " + ex.Message);
            }
        }

        //-------------------------------------------------------------
        //  AddStepResult
        /// <summary>
        /// Adds a single limit numeric step result. Save data in numlimitprop record with the LowLimit as "".
        /// </summary>
        /// <param name="propName">Property name</param>
        /// <param name="result">Result of Test</param>
        /// <param name="compOperator">Compare operator</param>
        /// <param name="limit">Limit value</param>
        /// <param name="value">Value that was read</param>
        /// <param name="units">Units of the value</param>
        /// <param name="format">Format of the value</param>
        /// <param name="reportText">Report text from the test</param>
        /// <param name="errorCode">Test error code</param>
        public void AddStepResult(string propName, string result, string compOperator, double limit, double value,
            string units, string format, string reportText, int errorCode)
        {
            Guid propResultID = Guid.NewGuid();
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase))
                StepFailed = true;

            // now create the PropResult record
            try
            {
                currentPropResultIndex++;
                SavePropResultTextRecord(ResultDatabaseData.StepResult.Last().id, propResultID, currentPropResultIndex, propName, type.type_numeric, result, value, units, format);
                if (result.Equals("FAIL", StringComparison.InvariantCultureIgnoreCase))
                {
                    ResultDatabaseData.StepResult.Last().stepResult = result;
                    ResultDatabaseData.StepResult.Last().errorCode = (int)errorCode;
                    ResultDatabaseData.StepResult.Last().errorMsg = reportText;
                }
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SavePropResultDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating PropResult record. " + ex.Message);
            }

            // now create the NumLimitProps record
            try
            {
                SaveNumLimitsPropsTextRecord(Guid.NewGuid(), propResultID, result, compOperator, 0, limit, units);
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SaveNumLimitsPropsDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating NumLimitProps record. " + ex.Message);
            }
        }

        //-------------------------------------------------------------
        //  AddStepResult(string propName, string result, string compOperator, string compString, string value, string reportText)
        /// <summary>
        /// Adds a string limit step result. Save data in stringprop record.
        /// </summary>
        /// <param name="propName">Property name</param>
        /// <param name="result">Result of Test</param>
        /// <param name="compOperator">Compare operator</param>
        /// <param name="compString">Limit value</param>
        /// <param name="value">Value that was read</param>
        /// <param name="reportText">Report text from the test</param>
        /// <param name="errorCode">Test error code</param>
        public void AddStepResult(string propName, string result, string compOperator, string compString, string value,
            string reportText, int errorCode)
        {
            Guid propResultID = Guid.NewGuid();
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase))
                StepFailed = true;

            // now create the PropResult record
            try
            {

                currentPropResultIndex++;
                SavePropResultTextRecord(ResultDatabaseData.StepResult.Last().id, propResultID, currentPropResultIndex, propName, type.type_string, result, value, "", "");
                if (result.Equals("FAIL", StringComparison.InvariantCultureIgnoreCase))
                {
                    ResultDatabaseData.StepResult.Last().stepResult = result;
                    ResultDatabaseData.StepResult.Last().errorCode = (int)errorCode;
                    ResultDatabaseData.StepResult.Last().errorMsg = reportText;
                }
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SavePropResultDBRecord(connection);

            }
            catch (Exception ex)
            {
                throw new Exception("Error creating PropResult record. " + ex.Message);
            }

            // now create the StringtProps record
            try
            {
                SaveStringPropTextRecord(Guid.NewGuid(), propResultID, result, compOperator, compString);
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SaveStringPropDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating StringProps record. " + ex.Message);
            }
        }

        //-------------------------------------------------------------
        //  AddStepResult
        /// <summary>
        /// Adds a string pass/fail result.
        /// </summary>
        /// <param name="propName">Property name</param>
        /// <param name="result">Result of Test</param>
        /// <param name="reportText">Report text from the test</param>
        /// <param name="errorCode">Test error code</param>
        public void AddStepResult(string propName, string result, string reportText, int errorCode)
        {
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase))
                StepFailed = true;

            // now create the PropResult record
            try
            {
                currentPropResultIndex++;
                SavePropResultTextRecord(ResultDatabaseData.StepResult.Last().id, Guid.NewGuid(), currentPropResultIndex, propName, type.type_pass_fail, result, "", "", "");
                if (result.Equals("FAIL", StringComparison.InvariantCultureIgnoreCase))
                {
                    ResultDatabaseData.StepResult.Last().stepResult = result;
                    ResultDatabaseData.StepResult.Last().errorCode = (int)errorCode;
                    ResultDatabaseData.StepResult.Last().errorMsg = reportText;
                }
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SavePropResultDBRecord(connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating PropResult record. " + ex.Message);
            }
        }

        //-------------------------------------------------------------
        //  AddStepResult
        /// <summary>
        /// Just updates the step status.
        /// </summary>
        /// <param name="result">Result of Test</param>
        /// <param name="reportText">Report text from the test</param>
        /// <param name="errorCode">Test error code</param>
        public void AddStepResult(string result, string reportText, int errorCode)
        {
            if (result.Equals("FAIL", StringComparison.OrdinalIgnoreCase))
            {
                StepFailed = true;
            }

            // update the result record
            try
            {
                ResultDatabaseData.StepResult.Last().stepResult = result;
                ResultDatabaseData.StepResult.Last().errorCode = (int)errorCode;
                ResultDatabaseData.StepResult.Last().errorMsg = reportText;

                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                {
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    var currentStepResultID = ResultDatabaseData.StepResult.Last().id;
                    cmd.CommandText = "UPDATE step_result SET step_result.result = @Result WHERE step_result.id = @ID";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Result", result);
                    cmd.Parameters.AddWithValue("@ID", ResultDatabaseData.StepResult.Last().id);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE step_result SET step_result.report_text = @ReportText WHERE step_result.id = @ID";
                    cmd.Parameters.AddWithValue("@ReportText", reportText);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE step_result SET step_result.error_code = @ErrorCode WHERE step_result.id = @ID";
                    cmd.Parameters.AddWithValue("@ErrorCode", errorCode);
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating result record. " + ex.Message);
            }
        }


        //-----------------------------------------------------------
        //  AddStepRef
        /// <summary>
        /// Will add a value and set the status as REF
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="refValue"></param>
        /// <param name="units"></param>
        /// <param name="refFormat"></param>
        /// <param name="reportText"></param>
        public void AddStepRef(string propName, string refValue, string units, string refFormat, string reportText)
        {
            Guid propResultID = Guid.NewGuid();
            // now create the PropResult record
            try
            {
                currentPropResultIndex++;
                SavePropResultTextRecord(ResultDatabaseData.StepResult.Last().id, propResultID, currentPropResultIndex, propName, type.type_string, "REF", refValue, units, refFormat);
                if (DBInfoRecordOption == DBRecordType.OnTheFly)
                    SavePropResultDBRecord(connection);

            }
            catch (Exception ex)
            {
                throw new Exception("Error creating PropResult record. " + ex.Message);
            }

        }

        private void WriteResultDataToTextFile(string filename)
        {
            if (!Directory.Exists(Path.GetDirectoryName(filename)))
                Directory.CreateDirectory(Path.GetDirectoryName(filename));

            using (StreamWriter rep = new StreamWriter(filename))
            {
                rep.WriteLine("SN = " + ResultDatabaseData.serialNumber);
                rep.WriteLine("--------------------------");
                rep.WriteLine("Date: " + ResultDatabaseData.testStartTime);
                rep.WriteLine("Run Time: " + ResultDatabaseData.executionTime);
                rep.WriteLine("Operator: " + ResultDatabaseData.testOperator);
                rep.WriteLine("Station: " + ResultDatabaseData.stationName);
                rep.WriteLine("Station Operation: " + ResultDatabaseData.testOperation);
                rep.WriteLine("HLA Product Code: " + ResultDatabaseData.hla_productcode);
                rep.WriteLine("Limit File: " + ResultDatabaseData.limitFileName + " " + ResultDatabaseData.limitFileVersion);
                rep.WriteLine("Program Version: " + ResultDatabaseData.programVersion);
                 rep.WriteLine("Workorder: " + ResultDatabaseData.workorder);
               rep.WriteLine("");
                rep.WriteLine("Test Status: " + ResultDatabaseData.result);
                rep.WriteLine("Error Code: " + ResultDatabaseData.errorCode + " step: " + ResultDatabaseData.error_stepnumber);
                rep.WriteLine("Error Message: " + ResultDatabaseData.errorMessage);
                rep.WriteLine("Error Step Name: " + ResultDatabaseData.error_stepname);
                rep.WriteLine("");

                foreach (var step in ResultDatabaseData.StepResult)
                {
                    rep.WriteLine("=========================");
                    rep.WriteLine(" Test Name: " + step.stepName);
                    rep.WriteLine(" Test Number: " + step.testNumber);
                    rep.WriteLine(" Step Number: " + step.stepIndex);
                    rep.WriteLine(" Step Type: " + step.stepType);
                    rep.WriteLine(" Test Start Time: " + step.stepStarttime);
                    rep.WriteLine(" Test Result: "  + step.stepResult);
                    rep.WriteLine(" Report text: " + step.reportText);
                    rep.WriteLine(" Error Code: " + step.errorCode);
                    rep.WriteLine(" Error Message: " + step.errorMsg);

                    foreach (var prop in step.PropResult)
                    {
                        rep.WriteLine("");
                        rep.WriteLine(" --------------");
                        rep.WriteLine("  Prop name: " + prop.name);
                        rep.WriteLine("  Prop type: " + prop.type);
                        if (prop.type != (int)type.type_pass_fail)
                            rep.WriteLine("  Prop data: " + prop.data);
                        rep.WriteLine("  Prop result: " + prop.result);

                        if (prop.NumlimitProp != null)
                        {
                            rep.WriteLine("");
                            rep.WriteLine("  +++++++++++++++++");
                            rep.WriteLine("    Operator: " + prop.NumlimitProp.compOperator);
                            rep.WriteLine("    High limit: " + prop.NumlimitProp.highLimit);
                            rep.WriteLine("    Low limit: " + prop.NumlimitProp.lowLimit);
                            rep.WriteLine("    Units: " + prop.NumlimitProp.units);
                            rep.WriteLine("    Status: " + prop.NumlimitProp.status);
                        }
                        if (prop.StringProp != null)
                        {
                            rep.WriteLine("");
                            rep.WriteLine("  +++++++++++++++++");
                            rep.WriteLine("    Operator: " + prop.StringProp.compOperator);
                            rep.WriteLine("    Comp String: " + prop.StringProp.compString);
                            rep.WriteLine("    Status: " + prop.StringProp.status);
                        }
                    }
                }
            }
        }

        private void WriteResultDataToDB()
        {
            using (MySqlConnection connection = new MySqlConnection())
            {
                MySqlCommand cmd;
                string mysqlConnectString = "Server=" + DBInfoServer + ";Database=" + DBInfoDatabase + ";uid=" + DBInfoUser + ";password=" + DBInfoPassword +
                                                ";" + "Connect Timeout=" + connectionTimeout + ";";
                connection.ConnectionString = mysqlConnectString;
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error opeing database. \n Server= " + DBInfoServer + " Database= " + DBInfoDatabase + " User= " + DBInfoUser + ". " + ex.Message);
                }

                cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO dut_result(id, serial_number, start_test_date, execution_time, operator, station, result, limit_file_version, " +
                                    "limit_file_name, program_version, error_code, error_message, test_operation, error_stepnumber, workorder, hla_productcode, " +
                                    "error_stepname, error_testnumber)" +
                                    "VALUES(@ID, @SN, @SD, @ExTime, @Operator, @Station, @Result, @LimitFileVersion, " +
                                    "@LimitFileName, @ProgramVersion, @ErrorCode, @ErrorMessage, @TestOperation, " +
                                    "@Error_stepnumber, @Workorder, @HLA_productcode, @ErrorStepName, @ErrorTestNumber)";
                cmd.Parameters.AddWithValue("@ID", ResultDatabaseData.id.ToString());
                cmd.Parameters.AddWithValue("@SN", ResultDatabaseData.serialNumber);
                cmd.Parameters.AddWithValue("@SD", ResultDatabaseData.testStartTime);
                cmd.Parameters.AddWithValue("@ExTime", (DateTime.UtcNow - ResultDatabaseData.testStartTime).TotalSeconds);
                cmd.Parameters.AddWithValue("@Operator", ResultDatabaseData.testOperator);
                cmd.Parameters.AddWithValue("@Station", ResultDatabaseData.stationName);
                cmd.Parameters.AddWithValue("@Result", ResultDatabaseData.result);
                cmd.Parameters.AddWithValue("@LimitFileVersion", ResultDatabaseData.limitFileVersion);
                cmd.Parameters.AddWithValue("@LimitFileName", ResultDatabaseData.limitFileName);
                cmd.Parameters.AddWithValue("@ProgramVersion", ResultDatabaseData.programVersion);
                cmd.Parameters.AddWithValue("@ErrorCode", ResultDatabaseData.errorCode);
                cmd.Parameters.AddWithValue("@ErrorMessage", ResultDatabaseData.errorMessage);
                cmd.Parameters.AddWithValue("@TestOperation", ResultDatabaseData.testOperation);
                cmd.Parameters.AddWithValue("@Error_stepnumber", ResultDatabaseData.error_stepnumber);
                cmd.Parameters.AddWithValue("@Workorder", ResultDatabaseData.workorder);
                cmd.Parameters.AddWithValue("@HLA_productcode", ResultDatabaseData.hla_productcode);
                cmd.Parameters.AddWithValue("@ErrorStepName", ResultDatabaseData.error_stepname);
                cmd.Parameters.AddWithValue("@ErrorTestNumber", ResultDatabaseData.error_testnumber);
                cmd.ExecuteNonQuery();

                foreach (var step in ResultDatabaseData.StepResult)
                {
                    SaveStepResultDBRecord(connection, step);
                    if (step.PropResult.Count != 0)
                    {
                        foreach (var prop in step.PropResult)
                        {
                            SavePropResultDBRecord(connection, prop);
                            if (prop.NumlimitProp != null)
                            {
                                SaveNumLimitsPropsDBRecord(connection, prop.NumlimitProp);
                            }
                            if (prop.StringProp != null)
                            {
                                SaveStringPropDBRecord(connection, prop.StringProp);
                            }
                        }
                    }
                }
            }
        }

    }
}
