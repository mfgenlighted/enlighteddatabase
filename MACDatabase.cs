﻿// MACDatabase.cs
//
//  Support functions for the MAC address database.
//
//  7/31/2018
//      supports the last_mac_used field
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace FixtureDatabase
{
    public enum MACErrorCodes
    {
        None,                   //  0 - no error
        Open,                   //  1 - database open error
        NUMBER_PER_Read,        //  2 - error reading NUMBER_PER field
        MAC_DataRead,           //  3 - error reading MAC data
        SN_Format,              //  4 - incorrect SN format
        MAC_Gen,                //  5 - error generating MACs
        LockingTable,           //  6 - error locking mac table
        MAC_NotValidForFamily,  //  7 - mac being used is not in the family pool
        MAC_AssignedToDiffSN,   //  8 - mac is assigned to a different sn
        MAC_Recording,          //  9 - error recording mac(s) in data base
        MAC_Mismatch,           //  10 - MAC does not match SN in database
        Not_Open,               //  12 - database not open when assumed it should be
        Last_MAC_Read,          //  13 - error reading the last mac used
        Read_StartStop,         //  14 - error reading the start/stop mac data
        MAC_OutOfRange,         //  15 - new mac is out of the start/stop range.
    }
    public sealed class MACDatabase : IDisposable
    {
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;

        private struct databaseInfoData
        {
            public string user;
            public string password;
            public string server;
            public string database;
        }


        private string mysqlConnectString = "Server=localhost;Database=macsused;uid=operator;password=;";

        private databaseInfoData databaseInfo = new databaseInfoData(); 
        private MySqlConnection connection = null;

        public MACDatabase(string server, string database, string user, string password)
        {
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";
            connection = new MySqlConnection(mysqlConnectString);

        }



        public void Dispose()
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// will check to see if the database can be opened.
        /// </summary>
        /// <returns>true = can open</returns>
        public bool CheckForDatabase()
        {
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening " + databaseInfo.database + " on server " + databaseInfo.server + ". " + ex.Message;
                LastErrorCode = (int)MACErrorCodes.Open;
                return false;
            }
            connection.Close();
            return true;
        }


        /// <summary>
        /// Will look for the next MAC from the family. Conditions that can exist:
        ///     1 - pcba SN passed, no MAC
        ///         -> assumed that this board has no MAC and needs a new one. Will check to
        ///             see if this pcba already has a MAC and will return it if found. If none found,
        ///             one will be generated and returned.
        ///     2 - pcba SN and MAC passed
        ///         -> assumes that it is a board that has a MAC assigned to it already. It will 
        ///             look for the pcba and verify that the MAC passed matches the database. 
        ///             If they match, all is good. If not, it will fail and return the database MAC.
        ///             If the pcba is not found at all, it will fail and the MAC will be return as blank.
        /// If a new MAC needs to be generated, the family table will be locked during the process of
        /// generating and recording of the new MAC.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        ///  2 - error reading NUMBER_PER field
        ///  3 - error reading MAC data
        ///  4 - MACErrorCodes.SN_Format, SN must be 15 characters
        /// 7  - MACErrorCodes.MAC_NotValidForFamily, mac is not in the range of the given family
        /// 8  - MACErrorCodes.MAC_AssignedToDiffSN, this mac is assigned to a different sn
        /// 9  - MACErrorCodes.MAC_Recording, there was a error recording the mac(s)
        ///  10 - MACErrorCodes.MAC_Mismatch, MAC not assigned to this SN
        /// 11 - MACErrorCodes.Not_Open, Assumed database should be open
        /// 12 - MACErrorCodes.Last_MAC_Read, Error reading the last mac used for a family
        /// 13 - MACErrorCodes.Read_StartStop, error readin the start/stop macs
        /// 14 - MACErrorCodes.MAC_OutOfRange, the mac to be used is out of the family range
        /// </remarks>
        /// <param name="family"></param>
        /// <param name="pcbaSN"></param>
        /// <param name="stationName"></param>
        /// <param name="MAC"></param>
        /// <param name="failStatus"></param>
        /// <returns>
        /// true - success
        /// false - error. check LastErrorCode and LastErrorMessage
        /// </returns>
        public bool GetAndRecordNextMac(string family, string pcbaSN, string stationName, ref List<string> MACList)
        {
            bool status = true;
            List<string> lclMAC = new List<string>();
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            int numberOfMacs = 0;
            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (pcbaSN.Length != 15)        // if the serial number not the correct lenght
            {
                LastErrorMessage = "Incorrect pcba SN format";
                LastErrorCode = (int)MACErrorCodes.SN_Format;
                return false;
            }

            lclMAC.Clear();
            MACList.Clear();
            if (MACList.Count == 0)        // if no MAC is passed, generate a MAC
            {
                if (DoesPcaSnExist(ref lclMAC, ref numberOfMacs, family, pcbaSN))     // see if pcba SN in database already
                    MACList = lclMAC;                                   // it is, so just give it back
                else                                                // else nothing found or error. Generator a MAC
                {
                    if (LastErrorCode != 0)         // if there was a error
                    {
                        return false;       // DoesPcaSnExist will set the error codes(1 - 3)
                    }
                    try
                    {
                        connection.Open();      // not doing error checking. assume if it passed DoesPcaSnExist it will pass here
                        cmd = connection.CreateCommand();
                        cmd.CommandText = string.Format("LOCK TABLE productinfo WRITE, " + family + " WRITE");
                        cmd.ExecuteNonQuery();
                        lclMAC = GetNextMac2(family, numberOfMacs, ref rdr, cmd);
                        if (lclMAC.Count == 0)      // GetNextMac2 will set LastErrorxxx if error
                        {
                            connection.Close();
                            return false;
                        }
                        RecordMAC2(lclMAC, family, pcbaSN, stationName, cmd);
                        MACList = lclMAC;
                        cmd.CommandText = "UNLOCK TABLES";
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)       // LastErrorCode and LastErrorMessage set by functions
                    {
                        cmd.CommandText = "UNLOCK TABLES";
                        cmd.ExecuteNonQuery();
                        connection.Close();
                        LastErrorMessage = "Error locking or getting next mac. " + ex.Message;
                        LastErrorCode = (int)MACErrorCodes.MAC_DataRead;
                        return false;
                    }
                }

            }
            else                            // there is a MAC passed for this pcba SN
            {
                if (DoesPcaSnExist(ref lclMAC, ref numberOfMacs, family, pcbaSN))     // see if pcba SN in database already
                {
                    if (lclMAC.ElementAt(0).Equals(MACList.ElementAt(0), StringComparison.CurrentCultureIgnoreCase))      // if they are the same
                    {
                        if (numberOfMacs == 2)      // if there are two macs, check the second one
                        {
                            if (lclMAC.ElementAt(1).Equals(MACList.ElementAt(1), StringComparison.CurrentCultureIgnoreCase))
                                return true;            // both macs match
                        }
                        else
                            return true;            // only one mac and it passes
                    }
                    else                                                                    // else they do not match
                    {
                        LastErrorMessage = string.Format("MAC mismatch between database MAC({0}) and pcba MAC({1}))", lclMAC.ElementAt(0), MACList.ElementAt(0));
                        LastErrorCode = (int)MACErrorCodes.MAC_Mismatch;
                        return false;
                    }
                }
            }
            return status;
        }


        /// <summary>
        /// Get the next mac for a family. connection must be open. uses Exception for errors. Assumes connection is open
        /// and cmd = connection.CreateCommand() done.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        /// 11 - MACErrorCodes.Not_Open, Assumed database should be open
        /// 12 - MACErrorCodes.Last_MAC_Read, Error reading the last mac used for a family
        /// 13 - MACErrorCodes.Read_StartStop, error readin the start/stop macs
        /// 14 - MACErrorCodes.MAC_OutOfRange, the mac to be used is out of the family range
        /// </remarks>
        /// <param name="familyName">mac family name to use</param>
        /// <param name="noOfMacks">number of macs to get. valid 1 or 2</param>
        /// <param name="rdr">reader for the mac database</param>
        /// <param name="cmd">command for the mac database</param>
        /// <returns>
        /// List of strings. If list is empty, there was a error. check LastErrorCode and LastErrorMessage
        /// </returns>
        private List<string> GetNextMac2(string familyName, int noOfMacks, ref MySqlDataReader rdr, MySqlCommand cmd)
        {
            string assignedMac = string.Empty;
            string assignedMac2 = string.Empty;
            string lastMac = string.Empty;
            UInt64 istartmac, iendmac;
            string startmac = string.Empty, endmac = string.Empty;
            UInt64 nextmac = 0;
            List<string> maclist = new List<string>();

            if (connection == null)                 // if the database has not been opened
            {
                LastErrorMessage = "MAC database has not been opened yet.";
                LastErrorCode = (int)MACErrorCodes.Not_Open;
                return maclist;
            }


            // last mac is in the productinfo.last_mac_used field.
            // If that field is empty, means that need to convert from the old method to the new method
            try
            {
                cmd.CommandText = "SELECT last_mac_used FROM productinfo WHERE PRODUCT_FAMILY = '" + familyName + "'";
                rdr = cmd.ExecuteReader();
                rdr.Read();
                lastMac = rdr.GetString(0);
                rdr.Close();
                if (lastMac == string.Empty)        // the table needs to be updated to new method
                {
                    cmd.CommandText = "SELECT mac FROM " + familyName + " ORDER BY mac desc limit 1";        // find last mac
                    rdr = cmd.ExecuteReader();
                    rdr.Read();
                    if (rdr.HasRows)    // found data
                    {
                        lastMac = rdr.GetString(0);
                        rdr.Close();
                        nextmac = Convert.ToUInt64(lastMac, 16);                    // last mac in database. make it a number
                        nextmac += 3;                                               // add 3 to to make sure that it is the last one
                        assignedMac = String.Format("{0:X12}", nextmac).ToUpper();  // make it a string
                        cmd.CommandText = "UPDATE productinfo SET last_mac_used =  '" + assignedMac + "' WHERE PRODUCT_FAMILY = '" + familyName + "'";
                        cmd.ExecuteNonQuery();
                    }
                    else        // even the family table is empty
                    {
                        rdr.Close();
                        cmd.CommandText = "SELECT START FROM productinfo WHERE PRODUCT_FAMILY = '" + familyName + "'";
                        rdr = cmd.ExecuteReader();
                        rdr.Read();
                        lastMac = rdr.GetString(0);
                        rdr.Close();
                        cmd.CommandText = "UPDATE productinfo SET last_mac_used =  '" + lastMac + "' WHERE PRODUCT_FAMILY = '" + familyName + "'";
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error reading last mac used. " + ex.Message;
                LastErrorCode = (int)MACErrorCodes.Last_MAC_Read;
                return maclist;
            }

            // make new mac
            nextmac = Convert.ToUInt64(lastMac, 16);                    // last mac in database. make it a number
            nextmac++;                                                  // get next aviablble mac
            assignedMac = String.Format("{0:X12}", nextmac).ToUpper();  // make it a string
            if (noOfMacks == 2)                                         // if two macs needed.
            {
                nextmac++;                                              // get next for mac2
                assignedMac2 = String.Format("{0:X12}", nextmac).ToUpper();

            }
            try
            {
                // verify that the next one is within the product range
                cmd.CommandText = "SELECT start, end FROM productinfo WHERE product_family = '" + familyName + "'";
                rdr = cmd.ExecuteReader();
                rdr.Read();
                startmac = rdr.GetString(0);
                endmac = rdr.GetString(1);
                rdr.Close();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error reading start/stop macs." + ex.Message;
                LastErrorCode = (int)MACErrorCodes.Read_StartStop;
                return maclist;
            }

            istartmac = Convert.ToUInt64(startmac, 16);
            iendmac = Convert.ToUInt64(endmac, 16);
            if ((nextmac > iendmac) || (nextmac < istartmac))
            {
                string msg = string.Format("The new mac is out of range. Start: {0} End: {1} Next:{2}", startmac, endmac, assignedMac);
                LastErrorMessage = msg;
                LastErrorCode = (int)MACErrorCodes.MAC_OutOfRange;
                return maclist;
            }
            // update the last mac used
            maclist.Add(assignedMac);
            if (noOfMacks == 2)
                maclist.Add(assignedMac2);
            cmd.CommandText = "UPDATE productinfo SET last_mac_used =  '" + maclist.Last() + "' WHERE PRODUCT_FAMILY = '" + familyName + "'";
            cmd.ExecuteNonQuery();
            return maclist;
        }

        /// <summary>
        /// Will record the MAC assignment. 
        /// Check LastErrorCode and LastErrorMessage for errors. Assumes open connection and cmd = connection.CreateCommand() done.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        /// 9  - MACErrorCodes.MAC_Recording, there was a error recording the mac(s)
        /// </remarks>
        /// <param name="mac">macs to record</param>
        /// <param name="familyName">family name of pool</param>
        /// <param name="pcaSN">pcba serial number</param>
        /// <param name="stationName">station name</param>
        /// <param name="cmd"></param>
        private void RecordMAC2(List<string> mac, string familyName, string pcaSN, string stationName, MySqlCommand cmd)
        {
            string assignedMac = string.Empty;
            string lastMac = string.Empty;
            string startmac = string.Empty, endmac = string.Empty;
            string cmdText;

            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            try
            {
                cmdText = "INSERT INTO " + familyName + "(mac, mac2, pcba_sn, creation_date, station) VALUES" +
                            "(@MAC, @MAC2, @PCA_SN, @CREATION_DATE, @STATION)";
                cmd.CommandText = cmdText;
                cmd.Parameters.AddWithValue("@MAC", mac.First());
                if (mac.Count == 2)
                    cmd.Parameters.AddWithValue("@MAC2", mac.ElementAt(1));
                else
                    cmd.Parameters.AddWithValue("@MAC2", string.Empty);
                cmd.Parameters.AddWithValue("@PCA_SN", pcaSN);
                cmd.Parameters.AddWithValue("@CREATION_DATE", DateTime.UtcNow);
                cmd.Parameters.AddWithValue("@STATION", stationName);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error inserting MAC record. " + ex.Message;
                LastErrorCode = (int)MACErrorCodes.MAC_Recording;
            }
        }

        /// <summary>
        /// Will see if the PCA Serial Number exists in the pool records.
        /// </summary>
        /// <param name="mac">if pca sn is found, this is the mac(s) assigned to it</param>
        /// <param name="numberOfMacs">number of macs this serial needs</param>
        /// <param name="poolName">the pool name of the product</param>
        /// <param name="pcaSN">serial number of the PCA mac is being assigned to</param>
        /// <remarks>
        /// LastErrorCodes
        ///  1 - database open error
        ///  2 - error reading NUMBER_PER field
        ///  3 - error reading MAC data
        /// </remarks>
        /// <returns>
        /// false = SN not found. mac will be string.empty
        ///          or error with macs database. Check LastErrorMessage
        /// true = SN was found. MAC(s) returned
        /// </returns>
        public bool DoesPcaSnExist(ref List<string> mac, ref int numberOfMacs, string poolName, string pcaSN)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            numberOfMacs = 0;
            mac.Clear();
            LastErrorMessage = string.Empty;

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error 'DoesPcaSnExist' opening database." + ex.Message;
                LastErrorCode = (int)MACErrorCodes.Open; 
                return false;
            }

            cmd = connection.CreateCommand();

            // see how many macs are assigned to this pool
            try
            {
                cmd.CommandText = "SELECT NUMBER_PER FROM productinfo WHERE PRODUCT_FAMILY=\"" + poolName + "\";";
                rdr = cmd.ExecuteReader();
                rdr.Read();     // got the number
                numberOfMacs = rdr.GetInt16(0);
                rdr.Close();
            }
            catch (Exception ex)
            {
                string errmsg = "Error 'DoesPcaSnExist' getting NUMBER_PER value from pool " + poolName + ". " + ex.Message;
                LastErrorMessage = errmsg;
                LastErrorCode = (int)MACErrorCodes.NUMBER_PER_Read;
                if (rdr != null)
                    rdr.Close();
                connection.Close();
                return false;
            }

            // see if pca sn exists 
            try
            {
                if (numberOfMacs == 1)
                    cmd.CommandText = "SELECT mac, pcba_sn FROM " + poolName + " WHERE pcba_sn=\"" + pcaSN + "\";";
                else
                    cmd.CommandText = "SELECT mac, mac2, pcba_sn FROM " + poolName + " WHERE pcba_sn=\"" + pcaSN + "\";";
                rdr = cmd.ExecuteReader();
                if (rdr.Read())             // if found a entry
                {
                    mac.Add(rdr.GetString(0));
                    if (numberOfMacs != 1)
                        mac.Add(rdr.GetString(1));
                    rdr.Close();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string errmsg = "Error 'DoesPcaSnExist' reading MACS. " + ex.Message;
                LastErrorMessage = errmsg;
                LastErrorCode = (int)MACErrorCodes.MAC_DataRead;
            }
            rdr.Close();
            connection.Close();
            return false;
        }

        //-------------------------------------------------------------
        //  DoesMACExist
        //
        //  Will see if the MAC exists in the pool records. Only looks
        //  for the 1st mac.
        //
        //
        // Parameters:
        //  string mac - mac to look for
        //  string poolName - the pool name of the product
        //  string pcaSN - if mac is found, the pcba sn assigned to it
        //
        // Return:
        //  true - mac was found. The sn is returned in pcbaSN.
        //  false - mac not found. pcbaSN will be string.empty
        //
        public bool DoesMACExist(string mac, string poolName, ref string pcbaSN)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            pcbaSN = string.Empty;

            if (connection == null)                 // if the database has not been opened
                throw new Exception("MAC database has not been opened yet.");
            cmd = connection.CreateCommand();

            // see if mac exists 
            try
            {
                cmd.CommandText = "SELECT mac, pcba_sn FROM " + poolName + " WHERE mac=\"" + mac + "\"";
                rdr = cmd.ExecuteReader();
                if (rdr.Read())             // if found a entry
                {
                    pcbaSN = rdr.GetString(1);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error looking for mac. " + ex.Message);
            }
            finally
            {
                rdr.Close();
            }

            return false;
        }


        //------------------------------------------------------------
        // IsValidMAC
        //
        //  Verifies the for the family, this MAC in in range.
        //
        // Parameters:
        //      string mac - mac to record
        //      string familyName - the family name of the product
        //
        // Return:
        //      true - is a mac that is in the range of the family
        //      false - mac is outside the range of the family
        //
        // Exceptions:
        //      MAC database has not been opened yet
        //      The Family name {0} is invalid
        //      The new mac is out of range. Start: {0} End: {1} Assigned:{2}
        //      Database errors
        private bool IsValidMAC(string mac, string familyName)
        {
            string assignedMac = string.Empty;
            string lastMac = string.Empty;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            UInt64 istartmac, iendmac, imac;
            string startmac = string.Empty, endmac = string.Empty;

            if (connection == null)                 // if the database has not been opened
                throw new Exception("MAC database has not been opened yet.");

            try
            {
                // verify that the next one is within the product range
                cmd.CommandText = "SELECT start, end FROM productinfo WHERE product_family = \"" + familyName + "\"";
                rdr = cmd.ExecuteReader();
                rdr.Read();
                startmac = rdr.GetString(0);
                endmac = rdr.GetString(1);
            }
            catch (Exception ex)
            {
                throw new Exception("DB error reading start/stop macs", ex);
            }
            finally
            {
                rdr.Close();
            }

            imac = Convert.ToUInt64(mac, 16);

            istartmac = Convert.ToUInt64(startmac, 16);
            iendmac = Convert.ToUInt64(endmac, 16);
            if ((imac > iendmac) || (imac < istartmac))
            {
                string msg = string.Format("The new mac is out of range. Start: {0} End: {1} Assigned:{2}", startmac, endmac, assignedMac);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks to see if this MAC exists for a family.
        /// </summary>
        /// <param name="mac">string - mac to look for</param>
        /// <param name="familyName">string - family name section to look in</param>
        /// <param name="pcaSN">ref string - if mac found, pcbaSN recorded</param>
        /// <returns>true = found the mac</returns>
        public bool IsMacADuplicate(string mac, string familyName, ref string pcaSN)
        {
            string assignedMac = string.Empty;
            string lastMac = string.Empty;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            string startmac = string.Empty, endmac = string.Empty;

                    // verify that it is not a dup. 
            try
            {
                cmd.CommandText = "SELECT mac, pcba_sn FROM " + familyName + " WHERE mac=\"" + mac + "\"";
                rdr = cmd.ExecuteReader();
                if (rdr.Read())             // if found a entry
                {
                    pcaSN = rdr.GetString(1);
                   return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error checking for dup mac", ex);
            }
            finally
            {
                rdr.Close();
            }
            pcaSN = string.Empty;
            return false;
        }

    }
}
