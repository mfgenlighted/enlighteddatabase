﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Data;

//
//  9-9-2014    LN
//      LookForPCBASN will now create a table in the database for a product code if it does
//      not exists.
//  12-8-2015 LN
//      Added LookForHLASN function
//  1-5-2017  LN
//      Added LastErrorMessage and LastErrorStatus.
//  7/24/2018
//      switching over to all_sns and last_hla_sn_used tables.
namespace FixtureDatabase
{
    public sealed class SerialNumberDatabase : IDisposable
    {
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;

        public enum SNErrorCodes
        {
            none,                   // 0  - no errors
            open,                   // 1  - database open error
            locking,                // 2  - error locking the table
            recordingData,          // 3  - error trying to record data
            not_open,               // 4 -  expected database to be open
            last_sn_read,           // 5  - error reading the last serial number
            dup_check,              // 6  - database error looking for duplicate sn
            dup_sn_found,           // 7  - duplicate sn was found
            reading,                // 8  - error reading data
            mac_find_db_error,      // 9  - error looking for mac in productcode table
        }

        private string outmsg = string.Empty;

        private struct databaseInfoData
        {
            public string user;
            public string password;
            public string server;
            public string database;
        }

        private databaseInfoData databaseInfo = new databaseInfoData();
        private string mysqlConnectString = string.Empty;
        private MySqlConnection connection = null;
        private bool disposed = true;

        private string stationName = string.Empty;


        public SerialNumberDatabase (string server, string database, string user, string password, string stationName)
        {
            this.stationName = stationName;
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";
            connection = new MySqlConnection(mysqlConnectString);
        }


        public void Dispose()
        {
            LastErrorMessage = string.Empty;
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                this.disposed = true;
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// will check to see if the database can be opened.
        /// </summary>
        /// <returns>true = can open</returns>
        public bool CheckForDatabase()
        {
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening " + databaseInfo.database + " on server " + databaseInfo.server  + ". " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }
            connection.Close();
            return true;
        }

        //---------------------------------------------------------------
        // GetAndRecordNextSN
        /// <summary>
        /// Will get the next availible SN for a given product family and
        /// record it.
        /// </summary>
        /// <remarks>
        /// It will use the current date to figure out the week/year part of the SN.
        ///
        ///      Generate the first part of the SN.
        ///          factoryCode productFamily version
        ///      if the lastunsed table exists
        ///         get the last serial number for that product code from the lastused table
        ///         update the lastused table with the new last used serial number
        ///      if the lastused table does not exist
        ///         Will then search the serial number database for the last sn issued for that sn group.
        ///      If that group does not exist, than use 00001.
        ///      Else add 1 to the highest sn # found.
        ///      
        /// The SerialNumberDatabase will be locked during this process.
        /// Returns "" if there is a problem.
        /// 
        /// LastErrorCodes
        /// 1 - SNErrorCodes.open, error opening database
        /// 2 - SNErrorCodes.locking, error locking table
        /// 3 - SNErrorCodes.recordingData, error recording sn data
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// 7 - SNErrorCodes.dup_sn_found, duplicate sn was found
        /// </remarks>
        /// <param name="factoryCode"></param>
        /// <param name="familyName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string GetAndRecordNextSN(string factoryCode, string familyName, string version,
                                        string pn, string pcbaSN, List<string> mac)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            string nextSN = string.Empty;
            outmsg = string.Empty;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            string fixedSNpart = string.Empty;
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            try
            {
                connection.Open();      // open the database
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Serial Number database open error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return string.Empty;
            }

            cmd = connection.CreateCommand();

            // lock the lastused table
            try
            {
                cmd.CommandText = string.Format("LOCK TABLE last_hla_sn_used WRITE, last_hla_sn_used AS FAMILY2 READ");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                LastErrorMessage = "Error locking table. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.locking;
                return string.Empty;
            }


            // see if this product code+version+date exists
            fixedSNpart = string.Format("{0}{1}{2}{3}{4:D2}", factoryCode, familyName, version, DateTime.Now.ToString("yy"), weekNum);
            cmd.CommandText = "SELECT sn_prefix, sn, last_creation_date FROM last_hla_sn_used WHERE sn_prefix = '" + fixedSNpart + "';";
            rdr = cmd.ExecuteReader();
            if (rdr.Read())            // if a entry was found
            {
                // found something, so replace it with the updated info
                nextSN = (Convert.ToInt32(rdr.GetString(1)) + 1).ToString("D5");
                rdr.Close();
                cmd.CommandText = "REPLACE INTO last_hla_sn_used(sn_prefix, sn, last_creation_date) " +
                    "VALUES(@SN_PREFIX, @SN, @LAST_CREATION_DATE)";
                cmd.Parameters.AddWithValue("@SN_PREFIX", fixedSNpart);
                cmd.Parameters.AddWithValue("@SN", nextSN);
                cmd.Parameters.AddWithValue("@LAST_CREATION_DATE", DateTime.UtcNow);
            }
            else            // nothing found, so start a new series
            {
                // new entry
                nextSN = "00000";
                rdr.Close();
                cmd.CommandText = "INSERT INTO last_hla_sn_used(sn_prefix, sn, last_creation_date) " +
                    "VALUES(@SN_PREFIX, @SN, @LAST_CREATION_DATE)";
                cmd.Parameters.AddWithValue("@SN_PREFIX", fixedSNpart);
                cmd.Parameters.AddWithValue("@SN", nextSN);
                cmd.Parameters.AddWithValue("@LAST_CREATION_DATE", DateTime.UtcNow);
            }
            cmd.ExecuteNonQuery();          // record into last_sn_used
            cmd.CommandText = "UNLOCK TABLES";
            cmd.ExecuteNonQuery();
            rdr.Close();
            cmd.Dispose();

            cmd = connection.CreateCommand();
            RecordSN2("all_sns", fixedSNpart + nextSN, pn, pcbaSN, mac, cmd);   // might already be recorded
            connection.Close();


            return fixedSNpart + nextSN;
        }


        /// <summary>
        /// Get the next serial number. Check LastErrorMessage and LastErrorCode for errors.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        /// 4  - SNErrorCodes.not_open, expected the database to be open
        /// 5  - SNErrorCodes.last_sn_read, error reading the last serial number in database
        /// </remarks>
        /// <param name="factoryCode"></param>
        /// <param name="familyName"></param>
        /// <param name="version"></param>
        /// <param name="rdr"></param>
        /// <param name="cmd"></param>
        /// <returns>
        /// serial number. If blank, there was a error.
        /// </returns>
        private string GetNextSN2(string factoryCode, string familyName, string version, ref MySqlDataReader rdr, ref MySqlCommand cmd)
        {
            string assignedSN = string.Empty;
            string lastSN = string.Empty;
            string nextSN = string.Empty;
            string fixedSNpart = string.Empty;
            cmd = connection.CreateCommand();
            int serialPart;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Serial Number database has not been opened yet.";
                LastErrorCode = (int)SNErrorCodes.not_open;
                return string.Empty;
            }

            // make the fixed part of the SN
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            fixedSNpart = string.Format("{0}{1}{2}{3}{4:D2}", factoryCode, familyName, version, DateTime.Now.ToString("yy"), weekNum);


            // see if this fixed part is in the database
            try
            {
                cmd.CommandText = "SELECT sn FROM " + familyName + " WHERE sn LIKE '" + fixedSNpart + "%' ORDER BY sn desc limit 1";
                rdr = cmd.ExecuteReader();
                if (rdr.Read())            // if a entry was found
                {
                    lastSN = rdr.GetString(0);
                }
                else            // nothing found, so start a new series
                {
                    lastSN = fixedSNpart + "00000";
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error getting new serial number. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.last_sn_read;
                return string.Empty;
            }
            finally
            {
                rdr.Close();
            }

            // make new SN
            serialPart = Convert.ToInt32(lastSN.Substring(10, 5));
            serialPart++;
            nextSN = string.Format("{0}{1:D5}", fixedSNpart, serialPart);

            return nextSN;
        }

        /// <summary>
        /// Record the serial number.
        /// </summary>
        /// <remarks>
        /// 3 - SNErrorCodes.recordingData, error recording sn data
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// 7 - SNErrorCodes.dup_sn_found, duplicate sn was found
        /// </remarks>
        /// <param name="sn"></param>
        /// <param name="pn"></param>
        /// <param name="pcaSN"></param>
        /// <param name="mac"></param>
        /// <param name="cmd"></param>
        /// <returns>
        /// true = ok, recorded
        /// false = dup or error
        /// </returns>
        private bool RecordSN2(string sn_table, string sn, string pn, string pcaSN, List<string> mac, MySqlCommand cmd)
        {
            string assignedSN = string.Empty;
            string lastSN = string.Empty;
            string cmdText;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Expected database to be open.";
                LastErrorCode = (int)SNErrorCodes.not_open;
                return false;
            }


            if (IsSNADuplicate(sn))        // verify that it is not a dup. 
            {
                return false;                           // there as a dup or error. IsSNADuplicate will set LastErrorxxxx
            }

            // now know that it is not a dup
            try
            {
                cmdText = "INSERT INTO all_sns (hla_sn, pcba_sn, creation_date, mac, mac2) " +
                    "VALUES(@SN,  @PCA_SN, @CREATION_DATE, @MAC, @MAC2)";
                cmd.Parameters.AddWithValue("@SN", sn);
                cmd.Parameters.AddWithValue("@PCA_SN", pcaSN);
                cmd.Parameters.AddWithValue("@CREATION_DATE", DateTime.UtcNow);
                if (mac.Count != 0)
                    cmd.Parameters.AddWithValue("@MAC", mac.ElementAt(0));
                else
                    cmd.Parameters.AddWithValue("@MAC", "");
                if (mac.Count == 2)
                    cmd.Parameters.AddWithValue("@MAC2", mac.ElementAt(1));
                else
                    cmd.Parameters.AddWithValue("@MAC2", "");
                cmd.CommandText = cmdText;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error inserting SN record. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.recordingData;
                return false;
            }
        }
        //--------------------------------------------
        // IsSNADuplicate
        //
        //  Checks to see if this hla SN exists.
        //
        // Parameters:
        //      string sn - sn to check
        //      string familyName - the family name of the product
        //
        // Return:
        //      true - sn was found
        //      false - no sn found or error
        /// <summary>
        /// Checks to see if this SN exists for a family. if return true, check LastErrorCode/LastErrorMessage for errors.
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// </remarks>
        /// <param name="sn"></param>
        /// <param name="familyName"></param>
        /// <returns>true = sn found or error</returns>
        private bool IsSNADuplicate(string sn)
        {
            string assignedMac = string.Empty;
            string lastMac = string.Empty;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            bool status;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Serial Number database has not been opened yet.";
                LastErrorCode = (int)SNErrorCodes.not_open;
                return true;
            }

            cmd = connection.CreateCommand();
            string startmac = string.Empty, endmac = string.Empty;

            // verify that it is not a dup. 
            cmd.CommandText = "SELECT hla_sn FROM all_sns WHERE hla_sn= '" + sn + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.Read())        // if found something
                {
                    status = true;
                    LastErrorCode = (int)SNErrorCodes.dup_sn_found;
                    LastErrorMessage = "SN " + sn + " is a duplicate.";
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "SN Dup check error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.dup_check;
                status = true;
            }
            return status;
        }

        /// <summary>
        /// Will look in the database to see if it can find the mac.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        /// 1 - SNErrorCodes.open
        /// 9 - SNErrorCodes.mac_find_db_error, error reading database for mac
        /// </remarks>
        /// <param name="productCode">product code section to look at</param>
        /// <param name="mac"></param>
        /// <param name="hlaSN"></param>
        /// <param name="hlaPN"></param>
        /// <param name="pcbaSN"></param>
        /// <returns>
        /// true = found the mac
        /// false = did not find mac or there was a error</returns>
        public bool LookForMac(string productCode, string mac, ref string hlaSN, ref string hlaPN, ref string pcbaSN)
        {
            bool status = true;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            string outmsg = string.Empty;

            hlaSN = string.Empty;
            hlaPN = string.Empty;
            pcbaSN = string.Empty;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            try
            {
                connection.Open();      // open the database
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Serial Number database open error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }

            cmd = connection.CreateCommand();

            // verify that it is not a dup. 
            cmd.CommandText = "SELECT sn, pn, pcba_sn FROM " + productCode + " WHERE mac= '" + mac + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.Read())        // if found something
                {
                    hlaSN = rdr.GetString(0);
                    hlaPN = rdr.GetString(1);
                    pcbaSN = rdr.GetString(2);
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error reading mac " + mac + " from productcode table " + productCode + ". " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.mac_find_db_error;
                status = false;
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
            }
            connection.Close();

            return status;
        }

        /// <summary>
        /// Will look to see if the PCBA SN exists in the database
        /// </summary>
        /// <remarks>
        /// Error codes
        /// 1   open error
        /// 8   reading data
        /// </remarks>
        /// <param name="productCode">HLA product code</param>
        /// <param name="pcbaSN"></param>
        /// <param name="hlaSN"></param>
        /// <param name="hlaPN"></param>
        /// <param name="mac"></param>
        /// <returns></returns>
        public bool LookForPCBASN(string pcbaSN, ref string hlaSN, ref string hlaPN)
        {
            bool status = true;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            hlaSN = string.Empty;
            hlaPN = string.Empty;

            try
            {
                connection.Open();      // open the database
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Serial Number database open error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }

            cmd = connection.CreateCommand();

            
            // look for the pcba sN
            cmd.CommandText = "SELECT hla_sn, pcba_sn FROM all_sns WHERE pcba_sn= '" + pcbaSN + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())        // just in case it has been assing multiple MACs. get the last
                    {
                        hlaSN = rdr.GetString(0);
                        hlaPN = rdr.GetString(1);
                         status = true;
                    }
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                MessageBox.Show("Look for pcba SN error. " + ex.Message);
                LastErrorMessage = "Look for pcba SN error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.reading;
                return false;
            }

            connection.Close();
            return status;
        }

        /// <summary>
        /// Will look to see if the HLA SN exists in the database
        /// </summary>
        /// <param name="productCode"></param>
        /// <param name="hlaSN"></param>
        /// <param name="pcbaSN"></param>
        /// <param name="mac"></param>
        /// <returns></returns>
        public bool LookForHLASN(string productCode, string hlaSN, ref string pcbaSN, ref string mac)
        {
            bool status = true;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            pcbaSN = string.Empty;
            mac = string.Empty;

            if (connection.State != System.Data.ConnectionState.Open)
                throw new Exception("Serial Number database has not been opened yet.");

            cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT sn, pcba_sn, mac FROM " + productCode + " WHERE sn= '" + hlaSN + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())        // just in case it has been assing multiple MACs. get the last
                    {
                        pcbaSN = rdr.GetString(1);
                        mac = rdr.GetString(2);
                        status = true;
                    }
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Look for hla SN error. " + ex.Message);
                throw new Exception("Look for hla SN error", ex);
            }

            return status;
        }

    }
}
