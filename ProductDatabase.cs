﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text.RegularExpressions;

/// <summary>
/// Reads productmap database.
/// </summary>
/// <remarks>
/// Version: 2.0
///     updated to new productmap.station stucture
/// Version: 2.1
///     added GetStationProductData(string stationCode, bool getActiveOnly, out List<Product_data> stationData, out string message)
/// Version:3.0
///     Added new fields to Product_data stucture and support code.
/// </remarks>
namespace FixtureDatabase
{
    // su_fun variables

    public sealed class ProductDatabase : IDisposable
    {
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;
        enum errorcodes
        {
            NoError,                    // 0
            DatabaseNotOpen,            // 1    Database is not open
            FunctionGroupInsertError,   // 2    error inserting data into the function groups
            ProductCodeRetrivingError,  // 3    error retreving a product code
            ProductCodeAddError,        // 4    error adding a product code
            ProductCodeDeleteError,     // 5    error deleting a product code
            FunctionGroupUpdateError,   // 6    error trying to update a function group
            StationDeleteError,         // 7    error trying to delete station data and attached product codes
        }

        public class Station_data
        {
            public Guid station_id;                           // group id
            public string name;
            public string function;
            public bool active;
            public string description;
            public string allowed_testprogram_version;
            public string cm_code;
            public DateTime modify_date;
            public string comment;
        }

        public class Product_data : ICloneable
        {
            public Guid product_code_id;
            public Guid station_id;
            public string product_code;                 // product code + version
            public string pcba_product_code;            // pcba product code + version
            public string model;
            public string pn;
            public string pcba_used;
            public string family_code;
            public int number_of_macs;
            public int hw_code;
            public string product_desc;
            public bool active_part;
            public string limit_version;            // limit version
            public string pfs_assembly_number;
            public string eco;
            public DateTime product_code_modify_date;
            public string product_code_comment;
            public string cm_codes;                      // cm codes that can see this product
            public object Clone()
            {
                Product_data newItem = (Product_data)this.MemberwiseClone();
                return newItem;
            }
        }
        private List<Product_data> Product_dataList;

        public struct ProductInfo
        {
            public string hlaproduct_code;
            public string hlaBasePN;
            public string pcbaproduct_code;
            public string pcbaBasePN;
            public string model;
        }

        private struct databaseInfoData
        {
            public string user;
            public string password;
            public string server;
            public string database;
        }

        private string mysqlConnectString = "Server=localhost;Database=macsused;uid=operator;password=;";

        private databaseInfoData databaseInfo = new databaseInfoData();
        private MySqlConnection connection = new MySqlConnection();
        private bool disposed = true;
       

        //------------------------------------------------------------
        // Open
        //  Open the database for use.
        //
        //  Parameters:
        //      string serever - server to connect to
        //      string database - database to connect to
        //      string user - database user to use
        //      string password - password for the user
        //
        public bool Open(string server, string database, string user, string password)
        {
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";

            connection.ConnectionString = mysqlConnectString;
            disposed = false;

            try
            {
                connection.Open();
            }
            catch (Exception msg)
            {
                databaseInfo.user = string.Empty;
                databaseInfo.password = string.Empty;
                databaseInfo.server = string.Empty;
                databaseInfo.database = string.Empty;
                mysqlConnectString = string.Empty;
                connection.Dispose();
                LastErrorMessage = msg.Message;
                return false;
            }

            return true;
        }

        //-----------------------------------------------------------
        // Close
        //  Close the open test database
        //
        public void Close()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            databaseInfo.user = string.Empty;
            databaseInfo.password = string.Empty;
            databaseInfo.server = string.Empty;
            databaseInfo.database = string.Empty;
            mysqlConnectString = string.Empty;
        }

        public void Dispose()
        {
            this.Close();
            connection.Dispose();
            this.disposed = true;
            GC.SuppressFinalize(this);
        }



        //----------------------------------------------------------------
        // GetProductInfo
        /// <summary>
        /// Will read the Product info from the Product database 
        /// </summary>
        /// <param name="testFunction"></param>
        /// <param name="product_code"></param>
        public void GetProductData(ref List<ProductInfo> productList)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            List<ProductInfo> pdataList = new List<ProductInfo>();
            ProductInfo pdata = new ProductInfo();

            if (connection.State != System.Data.ConnectionState.Open)
                throw new Exception("Product map database is not open.");

            cmd.CommandText = "SELECT hla_product_code, hla_base_part_number, pcba_product_code, pcba_base_part_number, model FROM hla_products";
            rdr = cmd.ExecuteReader();
            if (!rdr.HasRows)
                throw new Exception("Error reading product data.");

            while (rdr.Read())                                      // do for all the product codes in the table
            {
                pdata.hlaproduct_code = rdr.GetString(0);         // get the product code
                pdata.hlaBasePN = rdr.GetString(1);
                pdata.pcbaproduct_code = rdr.GetString(2);
                pdata.pcbaBasePN = rdr.GetString(3);
                pdata.model = rdr.GetString(4);
                pdataList.Add(pdata);       // add it to the list of product codes, local copy
            }
            rdr.Close();

            productList = pdataList;
        }

        //---------------------------------------------
        // GetStationProductionData
        /// <summary>
        /// Will get the active product codes for a given stationCode.
        /// </summary>
        /// <param name="stationCode">string - table within the Product Map database to get the data from</param>
        /// <param name="stationData">List,Product_data - product data list</param>
        public bool GetStationProductData(string stationCode, out List<Product_data> productData, out string message)
        {
            return GetStationProductData(stationCode, true, out productData, out message);
        }

        /// <summary>
        /// get product data with a filter. It will only return products that have this string as part of
        /// the product code.
        /// </summary>
        /// <param name="stationcode"></param>
        /// <param name="getActiveOnly"></param>
        /// <param name="productData"></param>
        /// <param name="productFilter"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool GetStationProductData(string stationcode, bool getActiveOnly, out List<Product_data> productData, string productFilter, out string message)
        {
            List<Product_data> fulllist = null;
            productData = new List<Product_data>();
            if (GetStationProductData(stationcode, getActiveOnly, out fulllist, out message))
            {
                foreach (var item in fulllist)
                {
                    if (item.product_code.ToUpper().Contains(productFilter.ToUpper()))
                        productData.Add(item);
                }
                return true;
            }
            return false;
        }

        public bool GetStationProductData(string stationCode, bool getActiveOnly, out List<Product_data> productData, out string message)
        { 
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            Product_dataList = new List<Product_data>();
            Product_data product_codeData = new Product_data();
            bool active_part = true;

            product_codeData.station_id = Guid.Empty; ;                     // 0
            product_codeData.product_code_id = Guid.Empty;                // 1
            product_codeData.product_code = string.Empty;                   // 2
            product_codeData.pcba_product_code = string.Empty;              // 3 optional
            product_codeData.model = string.Empty;                          // 4 optional
            product_codeData.pn = string.Empty;                             // 5
            product_codeData.pcba_used = string.Empty;                      // 6 optional
            product_codeData.family_code = string.Empty;                    // 7 optional
            product_codeData.product_desc = string.Empty;                   // 8
            product_codeData.active_part = false;                           // 9
            product_codeData.eco = string.Empty;                            // 10 optional
            product_codeData.product_code_modify_date = DateTime.MinValue;  // 11
            product_codeData.pfs_assembly_number = string.Empty;            // 12 optional
            product_codeData.limit_version = string.Empty;                  // 13 optional
            product_codeData.product_code_comment = string.Empty;           // 14 optional
            product_codeData.cm_codes = string.Empty;                        // 15


            message = string.Empty;
            productData = Product_dataList;             // empty list

            if (connection.State != System.Data.ConnectionState.Open)
            {
                message = "Product map database is not open.";
                return false;
            }

            cmd.CommandText = "SELECT function_groups.id, product_codes.id, product_codes.hla_product_code, " +
                                    "product_codes.pcba_product_code, product_codes.model, product_codes.hla_pn, product_codes.pcba_pn, product_codes.family_code, " +
                                    "product_codes.product_description, product_codes.active_part, product_codes.eco, product_codes.modify_date, " +
                                    "product_codes.pfs_assmebly_number, product_codes.limit_version, product_codes.comment, cm_codes " +
                                    //"FROM productmap.product_codes product_codes " +
                                    //"INNER JOIN productmap.function_groups function_groups " +
                                    "FROM product_codes product_codes " +
                                    "INNER JOIN function_groups function_groups " +
                                    "ON(product_codes.group_link = function_groups.id) " +
                                    "WHERE function_groups.name = '" + stationCode + "'";

            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                message = "Error reading product info: " + ex.Message;
                return false;
            }

            if (!rdr.HasRows)
            {
                message = string.Format("Error readying for station type {0}. No data found.", stationCode);
                rdr.Close();
                return false;
            }

            while (rdr.Read())                                      // do for all the product codes in the table
            {
                if (rdr.GetString(9).ToUpper().Equals("YES"))               // if YES
                    active_part = true;                                     // it is an active part
                else                                                        // else it is something other than NULL or YES
                    active_part = false;                                    // so not a active part
                if (!getActiveOnly)                                         // if !getActiveOnly, get all parts, active or not
                    active_part = true; 
                if (active_part)
                {
                    try     // just in case a non optional field is null
                    {
                        product_codeData.station_id = rdr.GetGuid(0);
                        product_codeData.product_code_id = rdr.GetGuid(1);
                        product_codeData.product_code = rdr.GetString(2);
                        if (!rdr.IsDBNull(3))
                            product_codeData.pcba_product_code = rdr.GetString(3);
                        if (!rdr.IsDBNull(4))
                            product_codeData.model = rdr.GetString(4);
                        product_codeData.pn = rdr.GetString(5);
                        if (!rdr.IsDBNull(6))
                            product_codeData.pcba_used = rdr.GetString(6);
                        if (!rdr.IsDBNull(7))
                            product_codeData.family_code = rdr.GetString(7);
                        product_codeData.product_desc = rdr.GetString(8);
                        if (!rdr.IsDBNull(13))
                            product_codeData.limit_version = rdr.GetString(13);
                        product_codeData.active_part = rdr.GetString(9).ToUpper().Contains("YES");
                        if (!rdr.IsDBNull(12))
                            product_codeData.pfs_assembly_number = rdr.GetString(12);
                        if (!rdr.IsDBNull(10))
                            product_codeData.eco = rdr.GetString(10);
                        product_codeData.product_code_modify_date = rdr.GetDateTime(11);
                        if (!rdr.IsDBNull(14))
                            product_codeData.product_code_comment = rdr.GetString(14);
                        product_codeData.cm_codes = rdr.GetString(15);
                    }
                    catch (Exception ex)
                    {
                        message = "A non optional field is missing. " + ex.Message;
                        rdr.Close();
                        return false;
                    }
                    Product_dataList.Add((Product_data)product_codeData.Clone());       // add it to the list of product codes, local copy
                }
            }
            rdr.Close();

            productData = Product_dataList;
            return true;
        }

        public bool GetListOfStations(ref List<Station_data> stationList, out string message)
        {
            return GetListOfStations(false, ref stationList, out message);
        }

        public bool GetListOfStations(bool activeOnly, ref List<Station_data> stationList, out string message)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            string value = string.Empty;
            Station_data stationItem;

            message = string.Empty;
            if (stationList == null)
                stationList = new List<Station_data>();

            if (connection.State != System.Data.ConnectionState.Open)
            {
                message = "Product map database is not open.";
                return false;
            }
                                 //   0   1     2         3       4            5                            6        7            8
            cmd.CommandText = "SELECT id, name, function, active, description, allowed_testprogram_version, cm_code, modify_date, comment FROM function_groups";
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                message = "Error reading station info: " + ex.Message;
                return false;
            }

            if (!rdr.HasRows)
            {
                message = string.Format("Error. No station data");
                rdr.Close();
                return false;
            }

            while (rdr.Read())                                      // do for all the product codes in the table
            {
                stationItem = new FixtureDatabase.ProductDatabase.Station_data();
                stationItem.station_id = rdr.GetGuid(0);
                stationItem.name = rdr.GetString(1);
                stationItem.function = rdr.GetString(2);
                stationItem.active = rdr.GetString(3).ToUpper().Contains("YES");
                if (!rdr.IsDBNull(4))
                    stationItem.description = rdr.GetString(4);
                if (!rdr.IsDBNull(5))
                    stationItem.allowed_testprogram_version = rdr.GetString(5);
                if (!rdr.IsDBNull(6))
                    stationItem.cm_code = rdr.GetString(6);
                stationItem.modify_date = rdr.GetDateTime(7);
                if (!rdr.IsDBNull(8))
                    stationItem.comment = rdr.GetString(8);
                if (activeOnly)
                {
                    if (stationItem.active)
                    {
                        if (stationList == null)
                            stationList = new List<FixtureDatabase.ProductDatabase.Station_data>();
                        stationList.Add(stationItem);
                    }
                }
                else
                {
                    if (stationList == null)
                        stationList = new List<FixtureDatabase.ProductDatabase.Station_data>();
                    stationList.Add(stationItem);
                }
            }
            rdr.Close();

            return true;
        }

        /// <summary>
        /// Delete the old data for the product code and replace it with the new data. If productData is null,
        /// ends up being a delete.
        /// </summary>
        /// <param name="stationCode"></param>
        /// <param name="productData"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool ReplaceStationProductData(Guid productCodeID, Product_data productData)
        {
            bool status = true;
            MySqlCommand cmd = null;
            MySqlDataReader rdr = null;
            cmd = connection.CreateCommand();
            string message = string.Empty;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Product map database is not open.";
                LastErrorCode = (int)errorcodes.DatabaseNotOpen;
                return false;
            }

            // make sure there is a record with that ID
            cmd.CommandText = "SELECT id FROM product_codes WHERE id = '" + productCodeID.ToString() + "'";
            try
            {
                rdr = cmd.ExecuteReader();
                rdr.Close();
            }
            catch (Exception)
            {
                LastErrorMessage = "Error trying to retrive data record";
                LastErrorCode = (int)errorcodes.ProductCodeRetrivingError;
                return false;
            }

            cmd.CommandText = "DELETE FROM product_codes WHERE id = '" + productCodeID.ToString() + "';";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LastErrorCode = (int)errorcodes.ProductCodeDeleteError;
                LastErrorMessage = "Error trying to delete product code. " + ex.Message + ":" + ex.InnerException.Message;
                return false;
            }

            if (productData != null)
            {
                if (!AddStationProductData(productData))        // error adding data
                {
                    LastErrorMessage = message;
                    LastErrorCode = (int)errorcodes.ProductCodeAddError;
                    status = false;
                }
            }

            return status;
        }

        /// <summary>
        /// Replace station data. If data is null, the station will be deleted along with any product code data.
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool ReplaceStationData(Guid stationID, Station_data data)
        {
            bool status = true;
            MySqlCommand cmd = null;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Product map database is not open.";
                LastErrorCode = (int)errorcodes.DatabaseNotOpen;
                return false;
            }

            if (data == null)       // if this a delete
            {
                //// must delete the product code data first
                cmd = connection.CreateCommand();
                //cmd.CommandText = "DELETE FROM product_codes WHERE group_link = '" + stationID + "'";
                //try
                //{
                //    cmd.ExecuteNonQuery();
                //}
                //catch (Exception ex)
                //{
                //    LastErrorMessage = "Error deleting product code attached to station code. " + ex.Message;
                //    if (ex.InnerException != null)
                //        LastErrorMessage += ":" + ex.InnerException.Message;
                //    LastErrorCode = (int)errorcodes.StationDeleteError;
                //    return false;
                //}

                // now can delete station data
                cmd.CommandText = "DELETE FROM function_groups WHERE id = '" + stationID + "'";
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LastErrorMessage = "Error deleting station code. " + ex.Message;
                    if (ex.InnerException != null)
                        LastErrorMessage += ":" + ex.InnerException.Message;
                    LastErrorCode = (int)errorcodes.StationDeleteError;
                    return false;
                }
            }
            else                    // must be a station item update
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE function_groups SET " +
                                    "name = '" + data.name + "'," +
                                    "function = '" + data.function + "'," +
                                    "active = '" + (data.active ? "YES" : "NO") + "'," +
                                    "description = '" + data.description + "'," +
                                    "allowed_testprogram_version = '" + data.allowed_testprogram_version + "'," +
                                    "cm_code = '" + data.cm_code + "'," +
                                    "comment = '" + data.comment + "' " +
                                    "WHERE id = '" + stationID + "'";
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    LastErrorMessage = "Error updating function code. " + ex.Message;
                    if (ex.InnerException != null)
                        LastErrorMessage += ":" + ex.InnerException.Message;
                    LastErrorCode = (int)errorcodes.FunctionGroupUpdateError;
                    status = false;
                }
            }
            return status;
        }

        public bool AddStationProductData(List<Product_data> productData)
        {
            bool status = true;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Product map database is not open.";
                LastErrorCode = (int)errorcodes.DatabaseNotOpen;
                return false;
            }

            Guid g = new Guid();
            foreach (var item in productData)
            {
                g = Guid.NewGuid();
                cmd.CommandText = "INSERT INTO product_codes(id, group_link, hla_product_code, " +
                                        "pcba_product_code, model, hla_pn, pcba_pn, family_code, " +
                                        "product_description, active_part, eco, modify_date, " +
                                        "pfs_assmebly_number, limit_version, comment, cm_codes) " +
                                        "VALUES(@ID, @GroupID, @HLAProductCode, " +
                                        "@PCBAProductCode, @Model, @HLAPN, @PCBAPN, @FamilyCode, " +
                                        "@ProductDesc, @ActivePart, @ECO, @ModifyDate, " +
                                        " @PFSAssmeblyNumber, @LimitVersion, @Comment, @CMCodes)";
                cmd.Parameters.AddWithValue("@ID", g.ToString());
                cmd.Parameters.AddWithValue("@GroupID", item.station_id);
                cmd.Parameters.AddWithValue("@HLAProductCode", item.product_code);
                cmd.Parameters.AddWithValue("@PCBAProductCode", item.pcba_product_code);
                cmd.Parameters.AddWithValue("@Model", item.model);
                cmd.Parameters.AddWithValue("@HLAPN", item.pn);
                cmd.Parameters.AddWithValue("@PCBAPN", item.pcba_used);
                cmd.Parameters.AddWithValue("@FamilyCode", item.family_code);
                cmd.Parameters.AddWithValue("@ProductDesc", item.product_desc);
                cmd.Parameters.AddWithValue("@ActivePart", item.active_part ? "YES":"NO");
                cmd.Parameters.AddWithValue("@ECO", item.eco);
                cmd.Parameters.AddWithValue("@ModifyDate", item.product_code_modify_date);
                cmd.Parameters.AddWithValue("@PFSAssmeblyNumber", item.pfs_assembly_number);
                cmd.Parameters.AddWithValue("@LimitVersion", item.limit_version);
                cmd.Parameters.AddWithValue("@Comment", item.product_code_comment);
                cmd.Parameters.AddWithValue("@CMCodes", item.cm_codes);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    LastErrorMessage = "Error trying to add new product data. " + ex.Message + ":" + ((ex.InnerException == null) ? "":ex.InnerException.Message);
                    LastErrorCode = (int)errorcodes.ProductCodeAddError;
                    return false;
                }
            }

            return status;
        }

        public bool AddStationProductData(Product_data productData)
        {
            bool status = true;
            List<Product_data> pdata = new List<FixtureDatabase.ProductDatabase.Product_data>();
            pdata.Add(productData);

            status = AddStationProductData(pdata);

            return status;

        }

        /// <summary>
        /// Add a Station entry. If Guid is empty on return, the entry was not added for some reason.
        /// Do CheckForErrors to find the error.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Guid AddStationData(Station_data data)
        {
            Guid newguid = Guid.Empty;
            MySqlCommand cmd = null;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Product map database is not open.";
                LastErrorCode = (int)errorcodes.DatabaseNotOpen;
                return newguid;
            }

            newguid = Guid.NewGuid();
            cmd = connection.CreateCommand();
            cmd.CommandText = "INSERT INTO function_groups(id, name, function, active, description, allowed_testprogram_version, cm_code, modify_date, comment) " +
                              "VALUES(@ID, @NAME, @FUNCTION, @ACTIVE, @DESCRIPTION, @ALLOWED, @CMCODES, @MODIFYDATE, @COMMENT)";
            cmd.Parameters.AddWithValue("@ID", newguid);
            cmd.Parameters.AddWithValue("@NAME", data.name);
            cmd.Parameters.AddWithValue("@FUNCTION", data.function);
            cmd.Parameters.AddWithValue("@ACTIVE", data.active ? "YES":"NO");
            cmd.Parameters.AddWithValue("@DESCRIPTION", data.description);
            cmd.Parameters.AddWithValue("@ALLOWED", data.allowed_testprogram_version);
            cmd.Parameters.AddWithValue("@CMCODES", data.cm_code);
            cmd.Parameters.AddWithValue("@MODIFYDATE", DateTime.Now);
            cmd.Parameters.AddWithValue("@COMMENT", data.comment);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error trying to add new product data. " + ex.Message;
                if (ex.InnerException != null)
                    LastErrorMessage += ":" + ex.InnerException.Message;
                LastErrorCode = (int)errorcodes.FunctionGroupInsertError;
                return Guid.Empty;
            }

            return newguid;
        }

        public int CheckForErrors(out string errormessage)
        {
            errormessage = LastErrorMessage;
            return LastErrorCode;
        }
    }
}
